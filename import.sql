# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.33.10 (MySQL 5.7.11)
# Database: divvyaws
# Generation Time: 2016-06-05 17:17:54 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories_genre
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories_genre`;

CREATE TABLE `categories_genre` (
  `category_genre_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_genre_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option_ID` int(10) unsigned NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_genre_ID`),
  KEY `categories_genre_option_id_foreign` (`option_ID`),
  CONSTRAINT `categories_genre_option_id_foreign` FOREIGN KEY (`option_ID`) REFERENCES `options_masters` (`option_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categories_genre` WRITE;
/*!40000 ALTER TABLE `categories_genre` DISABLE KEYS */;

INSERT INTO `categories_genre` (`category_genre_ID`, `category_genre_name`, `option_ID`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'Alternative Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(2,'Blues',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(3,'Classical Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(4,'Country Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(5,'Dance Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(6,'Easy Listening',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(7,'Electronic Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(8,'European Music (Folk / Pop)',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(9,'Hip Hop / Rap',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(10,'Indie Pop',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(11,'Grime',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(12,'Inspirational (incl. Gospel)',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(13,'Asian Pop (J-Pop, K-pop)',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(14,'Jazz',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(15,'Latin Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(16,'New Age',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(17,'Opera',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(18,'Pop (Popular music)',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(19,'R&B / Soul',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(20,'Reggae',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(21,'Rock',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(22,'Singer / Songwriter (inc. Folk)',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(23,'World Music / Beats',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(24,'Mystery',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(25,'Horror',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(26,'Health',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(27,'Children\'s',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(28,'History',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(29,'Poetry',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(30,'Art',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(31,'Action',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00');

/*!40000 ALTER TABLE `categories_genre` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cloud_urls
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cloud_urls`;

CREATE TABLE `cloud_urls` (
  `cloud_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `base_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cloud_type` enum('DEVELOPMENT','LIVE') COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cloud_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `cloud_urls` WRITE;
/*!40000 ALTER TABLE `cloud_urls` DISABLE KEYS */;

INSERT INTO `cloud_urls` (`cloud_ID`, `base_url`, `cloud_type`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'https://s3-eu-west-1.amazonaws.com/divviyaws','DEVELOPMENT','Y',NULL,NULL);

/*!40000 ALTER TABLE `cloud_urls` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2016_03_20_120214_create_user_master_table',1),
	('2016_03_20_120313_create_user_address_table',1),
	('2016_03_20_120323_create_user_cc_table',1),
	('2016_03_20_120334_create_user_password_table',1),
	('2016_03_20_120426_create_user_photo_bill_table',1),
	('2016_03_26_093647_create_options_masters_table',1),
	('2016_03_26_093726_create_categories_table',1),
	('2016_03_26_094740_create_upload_masters_table',1),
	('2016_03_26_094818_create_url_masters_table',1),
	('2016_03_26_104222_create_cloud_urls_table',1),
	('2016_04_30_115555_create_password_resets_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table options_masters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `options_masters`;

CREATE TABLE `options_masters` (
  `option_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`option_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `options_masters` WRITE;
/*!40000 ALTER TABLE `options_masters` DISABLE KEYS */;

INSERT INTO `options_masters` (`option_ID`, `option_name`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'MUSICS','Y',NULL,NULL),
	(2,'BOOKS','Y',NULL,NULL);

/*!40000 ALTER TABLE `options_masters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_id_index` (`email_id`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table payment_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `payment_options`;

CREATE TABLE `payment_options` (
  `payment_options_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_options` enum('PAYPAL') COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`payment_options_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `payment_options` WRITE;
/*!40000 ALTER TABLE `payment_options` DISABLE KEYS */;

INSERT INTO `payment_options` (`payment_options_id`, `payment_options`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'PAYPAL','Y',NULL,NULL);

/*!40000 ALTER TABLE `payment_options` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table upload_masters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `upload_masters`;

CREATE TABLE `upload_masters` (
  `upload_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_ID` int(10) unsigned NOT NULL,
  `category_genre_ID` int(10) unsigned NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `artist_author_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `album_title_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `album_price` decimal(5,2) NOT NULL,
  `track_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`upload_ID`),
  KEY `upload_masters_option_id_foreign` (`option_ID`),
  KEY `upload_masters_category_genre_id_foreign` (`category_genre_ID`),
  CONSTRAINT `upload_masters_category_genre_id_foreign` FOREIGN KEY (`category_genre_ID`) REFERENCES `categories_genre` (`category_genre_ID`),
  CONSTRAINT `upload_masters_option_id_foreign` FOREIGN KEY (`option_ID`) REFERENCES `options_masters` (`option_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `upload_masters` WRITE;
/*!40000 ALTER TABLE `upload_masters` DISABLE KEYS */;

INSERT INTO `upload_masters` (`upload_ID`, `option_ID`, `category_genre_ID`, `uploaded_by`, `artist_author_name`, `album_title_name`, `album_price`, `track_name`, `is_active`, `created_at`, `updated_at`)
VALUES
	(60025,2,24,1,'12','12',12.00,'','N','2016-05-16 21:23:12','2016-05-16 21:23:12'),
	(60026,1,1,1,'12','12',12.00,'','N','2016-05-16 21:33:23','2016-05-16 21:33:23'),
	(60027,2,24,1,'12','12',12.00,'','N','2016-05-16 21:34:53','2016-05-16 21:34:53'),
	(60028,1,4,11126,'beyonce','break your heart',12.00,'','Y','2016-05-26 18:43:33','2016-05-26 19:16:34'),
	(60029,2,24,11126,'selva','chinnakalai',12.00,'','N','2016-05-28 11:18:30','2016-05-28 11:18:30'),
	(60030,1,4,11126,'selva','test',12.00,'','Y','2016-05-28 11:18:54','2016-05-28 13:27:11'),
	(60031,1,1,11126,'test','test',12.00,'','N','2016-05-30 10:10:46','2016-05-30 10:10:46'),
	(60032,1,1,11126,'selv','stest',13.00,'','N','2016-05-30 10:19:54','2016-05-30 10:19:54'),
	(60033,1,8,11127,'beyonce ','run the world',13.00,'','Y','2016-06-02 21:22:37','2016-06-02 21:40:49');

/*!40000 ALTER TABLE `upload_masters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table url_masters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `url_masters`;

CREATE TABLE `url_masters` (
  `url_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url_params` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `upload_ID` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`url_ID`),
  KEY `url_masters_upload_id_foreign` (`upload_ID`),
  CONSTRAINT `url_masters_upload_id_foreign` FOREIGN KEY (`upload_ID`) REFERENCES `upload_masters` (`upload_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `url_masters` WRITE;
/*!40000 ALTER TABLE `url_masters` DISABLE KEYS */;

INSERT INTO `url_masters` (`url_ID`, `url_params`, `full_url`, `filename`, `upload_ID`, `created_at`, `updated_at`)
VALUES
	(6,'artwork','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/artwork/the_great_reset_book_cover.gif','the_great_reset_book_cover.gif',60030,'2016-05-28 13:21:15','2016-05-28 13:21:15'),
	(7,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/main/03 I Luv U (1).mp3','03 I Luv U (1).mp3',60030,'2016-05-28 13:22:56','2016-05-28 13:22:56'),
	(8,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/main/03 I Luv U (2).mp3','03 I Luv U (2).mp3',60030,'2016-05-28 13:24:35','2016-05-28 13:24:35'),
	(9,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/main/07 Cupid.m4a','07 Cupid.m4a',60030,'2016-05-28 13:25:41','2016-05-28 13:25:41'),
	(10,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/sample/14 JAY Z Blue.m4a','Blue1.m4a',60030,'2016-05-28 13:27:11','2016-05-28 13:27:11'),
	(11,'artwork','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/artwork/the_great_reset_book_cover.gif','the_great_reset_book_cover.gif',60030,'2016-05-28 13:21:15','2016-05-28 13:21:15'),
	(12,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/main/03 I Luv U (1).mp3','03 I Luv U (1).mp3',60030,'2016-05-28 13:22:56','2016-05-28 13:22:56'),
	(13,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/main/03 I Luv U (2).mp3','03 I Luv U (2).mp3',60030,'2016-05-28 13:24:35','2016-05-28 13:24:35'),
	(14,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/main/07 Cupid.m4a','07 Cupid.m4a',60030,'2016-05-28 13:25:41','2016-05-28 13:25:41'),
	(15,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/sample/14 JAY Z Blue.m4a','14 JAY Z Blue2.m4a',60030,'2016-05-28 13:27:11','2016-05-28 13:27:11'),
	(16,'artwork','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/artwork/the_great_reset_book_cover.gif','the_great_reset_book_cover.gif',60030,'2016-05-28 13:21:15','2016-05-28 13:21:15'),
	(17,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/main/03 I Luv U (1).mp3','03 I Luv U (1).mp3',60030,'2016-05-28 13:22:56','2016-05-28 13:22:56'),
	(18,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/main/03 I Luv U (2).mp3','03 I Luv U (2).mp3',60030,'2016-05-28 13:24:35','2016-05-28 13:24:35'),
	(19,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/main/07 Cupid.m4a','07 Cupid.m4a',60030,'2016-05-28 13:25:41','2016-05-28 13:25:41'),
	(20,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11126/60030/sample/14 JAY Z Blue.m4a','14 JAY Z Blue3.m4a',60030,'2016-05-28 13:27:11','2016-05-28 13:27:11'),
	(21,'artwork','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/artwork/Enchantment-Book-Cover-Best-Seller1.jpg','Enchantment-Book-Cover-Best-Seller1.jpg',60033,'2016-06-02 21:26:04','2016-06-02 21:26:04'),
	(22,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/main/01 Stand Up Tall.mp3','01 Stand Up Tall.mp3',60033,'2016-06-02 21:27:11','2016-06-02 21:27:11'),
	(23,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/main/16 Fix Up, Look Sharp (Instrumental).mp3','16 Fix Up, Look Sharp (Instrumental).mp3',60033,'2016-06-02 21:28:44','2016-06-02 21:28:44'),
	(24,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/sample/01 Stand Up Tall.mp3','01 Stand Up Tall.mp3',60033,'2016-06-02 21:29:45','2016-06-02 21:29:45'),
	(25,'artwork','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/artwork/Enchantment-Book-Cover-Best-Seller1.jpg','Enchantment-Book-Cover-Best-Seller1.jpg',60033,'2016-06-02 21:31:34','2016-06-02 21:31:34'),
	(26,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/sample/03 I Luv U.mp3','03 I Luv U.mp3',60033,'2016-06-02 21:31:39','2016-06-02 21:31:39'),
	(27,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/main/01 Stand Up Tall.mp3','01 Stand Up Tall.mp3',60033,'2016-06-02 21:33:12','2016-06-02 21:33:12'),
	(28,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/sample/06 Fix Up, Look Sharp.mp3','06 Fix Up, Look Sharp.mp3',60033,'2016-06-02 21:34:01','2016-06-02 21:34:01'),
	(29,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/sample/16 Fix Up, Look Sharp (Instrumental).mp3','16 Fix Up, Look Sharp (Instrumental).mp3',60033,'2016-06-02 21:35:27','2016-06-02 21:35:27'),
	(30,'main','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/main/16 Fix Up, Look Sharp (Instrumental).mp3','16 Fix Up, Look Sharp (Instrumental).mp3',60033,'2016-06-02 21:35:44','2016-06-02 21:35:44'),
	(31,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/sample/01 Stand Up Tall.mp3','01 Stand Up Tall.mp3',60033,'2016-06-02 21:36:35','2016-06-02 21:36:35'),
	(32,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/sample/03 I Luv U.mp3','03 I Luv U.mp3',60033,'2016-06-02 21:38:29','2016-06-02 21:38:29'),
	(33,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/sample/06 Fix Up, Look Sharp.mp3','06 Fix Up, Look Sharp.mp3',60033,'2016-06-02 21:39:22','2016-06-02 21:39:22'),
	(34,'sample','https://s3-eu-west-1.amazonaws.com/divviyaws/musics/11127/60033/sample/16 Fix Up, Look Sharp (Instrumental).mp3','16 Fix Up, Look Sharp (Instrumental).mp3',60033,'2016-06-02 21:40:49','2016-06-02 21:40:49');

/*!40000 ALTER TABLE `url_masters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_address
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_address`;

CREATE TABLE `user_address` (
  `address_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned NOT NULL,
  `line1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `town_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`address_ID`),
  KEY `user_address_user_id_foreign` (`user_ID`),
  CONSTRAINT `user_address_user_id_foreign` FOREIGN KEY (`user_ID`) REFERENCES `user_master` (`user_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_address` WRITE;
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;

INSERT INTO `user_address` (`address_ID`, `user_ID`, `line1`, `line2`, `town_city`, `country`, `zipcode`, `home_phone`, `mobile_phone`, `is_active`, `created_at`, `updated_at`)
VALUES
	(16,1,'58','Terry Road','Coventry, West Midlands','GB','cv12ba','','0754165042','Y','2016-05-10 22:42:39','2016-05-10 22:43:28'),
	(11156,11126,'58','Terry Road','Coventry, West Midlands','GB','cv12ba','','074584165042','Y','2016-05-14 12:35:15','2016-05-22 12:40:26'),
	(11157,11127,'58 ','','coventry, west midlands','GB','cv12ba','','123343','Y','2016-05-30 10:50:00','2016-06-05 14:42:10'),
	(11160,11130,'6 sandmere','','Birmingham','GB','b144jd','','07944936779','N','2016-05-30 11:11:13','2016-05-30 11:11:13'),
	(11161,11131,'6 sandmere','','Birmingham','GB','b144jd','','07944936779','N','2016-05-30 11:16:22','2016-05-30 11:16:22');

/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_cc
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_cc`;

CREATE TABLE `user_cc` (
  `user_cc_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned NOT NULL,
  `payment_options_id` int(10) unsigned NOT NULL,
  `card_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_code` int(11) NOT NULL,
  `sort_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_cc_ID`),
  KEY `user_cc_user_id_foreign` (`user_ID`),
  CONSTRAINT `user_cc_user_id_foreign` FOREIGN KEY (`user_ID`) REFERENCES `user_master` (`user_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_cc` WRITE;
/*!40000 ALTER TABLE `user_cc` DISABLE KEYS */;

INSERT INTO `user_cc` (`user_cc_ID`, `user_ID`, `payment_options_id`, `card_name`, `card_number`, `security_code`, `sort_code`, `account_number`, `expiry_date`, `email`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,11127,1,'','',0,'','','','selvac@hotmail.co.uk','Y','2016-06-05 17:06:55','2016-06-05 17:10:39');

/*!40000 ALTER TABLE `user_cc` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_master
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_master`;

CREATE TABLE `user_master` (
  `user_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` enum('MALE','FEMALE') COLLATE utf8_unicode_ci NOT NULL,
  `origanisation` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_ID`),
  UNIQUE KEY `user_master_email_id_unique` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_master` WRITE;
/*!40000 ALTER TABLE `user_master` DISABLE KEYS */;

INSERT INTO `user_master` (`user_ID`, `first_name`, `last_name`, `email_id`, `date_of_birth`, `gender`, `origanisation`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'selvaganapathy','chinnakalai','selvacganapathy@gmail.com','1985-01-06','MALE','scube software solutions','Y','2016-03-26 11:24:10','2016-03-26 11:24:10'),
	(11126,'selva','chinnakalai','scubessapp@gmai.com','1985-05-08','MALE','Scube software solutions ltd','Y','2016-05-14 12:35:15','2016-05-22 12:40:26'),
	(11127,'selva','chinnakalai','selvac@hotmail.co.uk','1986-01-06','MALE','scube software solutions lts','Y','2016-05-30 10:50:00','2016-06-05 14:42:10'),
	(11130,'Duane','Williams','duane.williams@divviy.com','1985-09-09','MALE','Divviy','N','2016-05-30 11:11:13','2016-05-30 11:11:13'),
	(11131,'Duane','Williams','duane@divviy.com','1985-09-09','MALE','Divviy','N','2016-05-30 11:16:22','2016-05-30 11:16:22');

/*!40000 ALTER TABLE `user_master` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_password
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_password`;

CREATE TABLE `user_password` (
  `pwd_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_ID` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` enum('ADMIN','GENERAL','CLIENT') COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pwd_ID`),
  UNIQUE KEY `user_password_email_id_unique` (`email_id`),
  CONSTRAINT `user_password_email_id_foreign` FOREIGN KEY (`email_id`) REFERENCES `user_master` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_password` WRITE;
/*!40000 ALTER TABLE `user_password` DISABLE KEYS */;

INSERT INTO `user_password` (`pwd_ID`, `email_id`, `remember_token`, `user_ID`, `password`, `user_type`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'selvacganapathy@gmail.com','sAnGiPINPuB1Y9pkXoq8b8k59YKwHMOq1WzsJrw7f9XvAiE7c57zeKoCdum2',1,'$2y$10$owyHUyqzf2DHAGqsf/k5k.UqGtk7eajpyxYi6C8ZfZtWKbM/a3gpa','ADMIN','Y','2016-04-24 13:59:10','2016-05-28 13:28:11'),
	(11136,'scubessapp@gmai.com','zZz1dMcc8WABUODbIt7MENTLW3TwAciOw3eyQA2xBUCzxeJvRwXEmYnulU0H',11126,'$2y$10$owyHUyqzf2DHAGqsf/k5k.UqGtk7eajpyxYi6C8ZfZtWKbM/a3gpa','CLIENT','Y','2016-05-14 12:35:15','2016-06-05 10:31:42'),
	(11137,'selvac@hotmail.co.uk','LxBRDIcUCVtNzMT39tGqdkJWFOAwlYEdHVg8ssDJK26HtKngw9JSOoJGD2YG',11127,'$2y$10$B.bAzH.KqHTRHDIrWkQtHuPhOl4GvLli.OO/er2ovBoCgNQ08eB3O','CLIENT','Y','2016-05-30 10:50:00','2016-06-05 17:11:19'),
	(11140,'duane.williams@divviy.com','',11130,'$2y$10$ZpmJwNzis0RVeGyuw7mS3.1GfFSfmvB3eXzm4Xl7t/IS5Rc8k2uN6','CLIENT','N','2016-05-30 11:11:13','2016-05-30 11:11:13'),
	(11141,'duane@divviy.com','',11131,'$2y$10$svgwugQNZOuKnwYqBungPe3e8L8H8TvOJFnIrAb8OGAWCEIGxO/7a','CLIENT','N','2016-05-30 11:16:22','2016-05-30 11:16:22');

/*!40000 ALTER TABLE `user_password` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_photo_bill
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_photo_bill`;

CREATE TABLE `user_photo_bill` (
  `upload_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned NOT NULL,
  `utility_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`upload_ID`),
  KEY `user_photo_bill_user_id_foreign` (`user_ID`),
  CONSTRAINT `user_photo_bill_user_id_foreign` FOREIGN KEY (`user_ID`) REFERENCES `user_master` (`user_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_photo_bill` WRITE;
/*!40000 ALTER TABLE `user_photo_bill` DISABLE KEYS */;

INSERT INTO `user_photo_bill` (`upload_ID`, `user_ID`, `utility_url`, `photo_url`, `is_active`, `created_at`, `updated_at`)
VALUES
	(11146,11126,'SampleElectricBill.pdf','sample-customer-photo2.jpg','Y','2016-05-14 12:35:15','2016-05-22 12:40:26'),
	(11147,11127,'SampleElectricBill.pdf','imagesttt.jpg','N','2016-05-30 10:50:00','2016-05-30 10:50:00'),
	(11150,11130,'Two-Bill-Example.pdf','imagesttt.jpg','N','2016-05-30 11:11:13','2016-05-30 11:11:13'),
	(11151,11131,'Two-Bill-Example.pdf','imagesttt.jpg','N','2016-05-30 11:16:22','2016-05-30 11:16:22');

/*!40000 ALTER TABLE `user_photo_bill` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
