@extends('includes.default')

@section('content')
<div class="panel-body">
	<div class="col-sm-6 col-md-6 col-lg-6" style="height:450px;text-align:center;">
			<img class="login_banner_image" src="{{asset('images/main_image_login.png')}}">
    </div>
	<div class="col-sm-6 col-md-6 col-lg-6">

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
			<a href="#" ><img class="logo" src={{asset('images/logo.png')}} alt="Logo"></a><br><br>
			{!! csrf_field() !!}
			<div class="form-group">
				<label class="col-md-4 control-label">E-Mail Address</label>
				<div class="col-md-6">
					<input type="email" class="form-control" name="email_id" value="{{ old('email') }}">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4 control-label">Password</label>
				<div class="col-md-6">
					<input type="password" class="form-control" name="password">
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					<div class="checkbox">
						<label>
							<input type="checkbox" name="remember"> Remember Me
						</label>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					<button type="submit" class="btn btn-primary">Login</button>
					<a class="btn btn-link" href="{{ url('/password/email') }}">Can't access your account?</a>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
						<a class="btn btn-primary" href="{{ url('/auth/register') }}"> Sign up </a> here to become an Divviyan artist<br>
				</div>
			</div>
		</form>
		<hr>
		<div class="form-group applink" styel="text-align:center;">Sign up as a user via App Store or Google Play
		</div>	
		<div class="applinks">
			<a href="#" ><img class="logo" src={{asset('images/coming-soon-app-store.png')}} alt="Logo"></a>
			<a href="#" ><img class="logo" src={{asset('images/coming-soon-google-store.png')}} alt="Logo"></a>
		</div>
	</div>
</div>
@endsection
