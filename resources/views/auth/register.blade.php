@extends('includes.default')

@section('content')
<div class="panel-heading ">DIVVIY REGISTRATION FORM</div>
	<div class="panel-body">
@if ($errors->has())
    <div class="paragraph" style="color:#990038;">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li> 
        @endforeach
    </div>
@endif
@if(Session::has('flash_message'))
    <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
@endif
<br>
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}" enctype="multipart/form-data"  onsubmit="return checkForm(this);">
			{!! csrf_field() !!}
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> First Name: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Last Name: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Organisation: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="origanisation" value="{{ old('origanisation') }}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Mobile Phone: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="mobile_phone" value="{{ old('mobile_phone') }}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Home Telephone: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="home_phone" value="{{ old('home_phone') }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Date of Birth: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="date_of_birth" value="{{ old('date_of_birth') }}" id="datepicker" required>
					</div>
				</div>							
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Gender: </label>
					<div class="col-md-8">
						<select value="{{ old('gender') }}" name="gender" required>
							<option value="MALE">Male</option>
							<option value="FEMALE">Female</option>
						</select> 
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> E-Mail Address: </label>
					<div class="col-md-8">
						<input type="email" class="form-control" name="email_id" value="{{ old('email_id') }}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Confirm E-Mail: </label>
					<div class="col-md-8">
						<input type="email" class="form-control" name="email_id_confirmation" value="{{ old('email_id_confirmation') }}" required>
					</div>
				</div>								
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> ZIP/Post Code: </label>
					<div class="col-md-8">
						<div class="input-group">
						<input type="text" class="form-control" id="zipcode" name="zipcode" value="{{ old('zipcode') }}" required>
						<span class="input-group-addon" id="basic-addon2">
							<a id="postCodeBtn" href="#">
								<span class="glyphicon glyphicon-search"  aria-hidden="true" id="postCodeSearch" style="font-size:15px;color:#000;"></span>
								<i class="fa fa-refresh fa-spin" id="postCodeRefresh" style="font-size:16px;color:#000;display:none;"></i>
							</a>
						</span>
						</div>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-md-4 control-label"><span class="asterick">*</span> Address Line 1: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="line1" name="line1" value="{{ old('line1') }}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Address Line 2: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="line2" name="line2" value="{{ old('line2') }}">
					</div>
				</div>				
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Town: </label>
					<div class="col-md-8">
						<input type="text" class="form-control" id="town_city" name="town_city" value="{{ old('town_city') }}" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><span class="asterick">*</span> Country: </label>
					<div class="col-md-8">
						<select id="country" name="country" required>
							<option value="GB" >United Kingdom</option>
							<option value="UM">U.S. Minor Outlying Islands</option>
							<option value="PU">U.S. Miscellaneous Pacific Islands</option>
							<option value="VI" class="select-hr">U.S. Virgin Islands</option>
							@foreach(Countries::getList('en', 'php', 'cldr') as $key=>$value)
  							<option value="{{ $key }}">{{ $value }}</option>
  							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Password: </label>
					<div class="col-md-8">
						<input type="password" class="form-control" name="password" required>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Confirm Password: </label>
					<div class="col-md-8">
						<input type="password" class="form-control" name="password_confirmation" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Upload Photo: </label>
					<input id="user_photo_field" disabled="disabled"/>
					<div class="fileUpload btn btn-primary">
			    		<span>Upload</span>
			    		<input id="user_photo" type="file" class="upload" name="user_photo" accept="image/*" required/>
					</div>
					<label id="uploadPhoto" placeholder="Choose File" disabled="disabled" /></label>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> <span class="asterick">*</span> Upload Bill: </label>
					<input id="user_bill_field" disabled="disabled"  />
					<div class="fileUpload btn btn-primary">
			    		<span>Upload</span>
			    		<input id="user_bill" type="file" class="upload" name="user_bill"  accept="image/*,application/pdf" required/>
					</div>							
						<label id="uploadBill" placeholder="Choose File" disabled="disabled" /> </label>
				</div>
			</div>
			<p><input type="checkbox" name="terms"> By clicking Register, you agree to our  <a target="_blank" href="{{ asset('terms/Terms_and_conditions_v1.pdf') }}"><u>Terms and Conditions</u></a>, including our Cookie Use.</p>
			<div class="form-group">	
				<button type="submit" class="btn btn-primary register">Register</button>
			</div>
		</form>
</div>		
<br><br>
@endsection
