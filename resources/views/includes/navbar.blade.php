	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="/"><img class="logo" src={{asset('images/logo.png')}} alt="Logo"></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
					@if(Request::path() == 'auth/register')
					<li><a href="{{ url('/') }}">LOGIN</a></li>
					@endif
			</ul>
			@if((Request::path() !== '/') AND (Request::path() !== 'login') AND (Request::path() !== 'logout') AND (Request::path() !== 'auth/register') AND (Request::path() !== 'password/email'))  
			<div class="collapse navbar-collapse" id="navbar">

				<ul class="nav navbar-nav navbar-right">
					@if(Request::path() == '/auth/register'))
					<li><a href="{{ url('/') }}">LOGIN</a></li>
					@endif
					<li><a href="{{ url('/home') }}"><img class="navicons home" src={{asset('images/home.png')}} alt="home" title="home"></a></li>
					<li><a href="{{ url('/feed') }}"><img class="navicons feed" src={{asset('images/feed.png')}} alt="home" title="feed" ></a></li>
					<li><a href="{{ url('/upload') }}"><img class="navicons upload" src={{asset('images/upload.png')}} alt="home" title="upload"></a></li>
					@if(Auth::user()->user_type == 'ADMIN')
					<li class="dropdown">
					<a href="{{ url('/admin') }}" class="dropdown-toggle user" data-toggle="dropdown"><img class="navicons" src={{asset('images/admin.png')}} alt="home" title="admin"></a>
						<ul class="dropdown-menu">
            				<li><a href="{{ url('/admin') }}">New members</a></li>
            				<li><a href="{{ url('/admin/approved') }}">All members</a></li>
        				</ul>
        			</li>
					@endif
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img class="navicons" src={{asset('images/user.png')}} alt="home" title="user profile"></a>
						<ul class="dropdown-menu">
            				<li><a href="{{ url('/edit/'.Auth::user()->user_ID.'') }}"><img class="navicons small" src="{{asset('images/edit_profile.png') }}"> <span> edit</span></a></li>
							<li><a href="{{ url('/logout') }}"> <img class="navicons small" src="{{asset('images/logout.png') }}"> <span> logout</span></a></li>        				
						</ul>
        			</li>
				</ul>
			</div>
@endif
		</div>
	</nav>
	<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">