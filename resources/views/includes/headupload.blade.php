<!DOCTYPE HTML>
<!--
/*
 * jQuery File Upload Plugin jQuery UI Demo
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
-->
<html lang="en">
<head>
<!-- Force latest IE rendering engine or ChromeFrame if installed -->
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<meta charset="utf-8">
<title>DIVVIY</title>
<meta name="description" content="File Upload widget with multiple file selection, drag&amp;drop support, progress bars, validation and preview images, audio and video for jQuery. Supports cross-domain, chunked and resumable file uploads and client-side image resizing. Works with any server-side platform (PHP, Python, Ruby on Rails, Java, Node.js, Go etc.) that supports standard HTML form file uploads.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<!-- jQuery UI styles -->
<link href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/jquery.fileupload.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.fileupload-ui.css') }}">

<!-- Demo styles {{ asset('css/jquery.fileupload-ui-noscript.css') }} -->
<!-- <link rel="stylesheet" href="{{ asset('css/demo.css') }}"> -->
<link rel="stylesheet" href="{{ asset('css/navwizard.css') }}"> 

<!--[if lte IE 8]>
<link rel="stylesheet" href="css/demo-ie8.css">
<![endif]-->
<style>
/* Adjust the jQuery UI widget font-size: */
.ui-widget {
    font-size: 0.95em;
}
</style>

<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="{{ asset('css/jquery.fileupload-noscript.css') }}"></noscript>
<noscript><link rel="stylesheet" href="{{ asset('css/jquery.fileupload-ui-noscript.css') }}"></noscript>

<style type="text/css">

@font-face { 
    font-family:Muli; 
    src: url('{{ asset('fonts/Muli.ttf') }}');
}
@font-face { 
    font-family:gillsans; 
    src: url('{{ asset('fonts/gillsans.ttf') }}');
}
@font-face { 
    font-family:SourceSans; 
    src: url('{{ asset('fonts/SourceSansPro-Light.ttf') }}');
}
body {

    font-family:SourceSans;
    width: 80%;
    float: none;
    margin: 0 auto;
}
.fade {
    opacity: 1;
    transition: opacity 0.15s linear 0s;
    padding-left: 0;
}
.logo {
    width: 120px;
}
.panel-heading {
    text-align: center;
}
.form-group {
    margin: 5px;
}
.form-control {
    height: 25px;
    font-size: 12px;
}
.btn.btn-primary {
    background: #93e7e7 none repeat scroll 0 0;
    border: 0 none;
    color: black;
    font-weight: bold;
    text-transform: uppercase;
}
.panel-default {
    border: 0px;
    background: transparent;
}
.panel-default > .panel-heading {
    background-color: transparent;
    border: 0px;
    font-size: 22px;
    font-weight: bold;
}
.col-md-12 {
    background: #FAFAFA;
}
nav {
    background: transparent none repeat scroll 0 0 !important;
    border: 0 none !important;
}
.form-control {
    background: #eff5f7 none repeat scroll 0 0;
    border-color: #e1f0f4;
    font-family: Arial;
    height: 30px;
}
ul {
    font-size: 14px;
    padding-top: 10px;
}
label {
    text-align: left !important;
    font-size: 12px;
}
.fileUpload {
    position: relative;
    overflow: hidden;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
button, select {
    background: #93e7e7 none repeat scroll 0 0;
    border: 0 none !important;
    text-transform: uppercase;
}
.setBottomCenter {
    bottom: 0 !important;
    margin-bottom: 10px;
    position: absolute;
    right: 0%;
    text-align: center;
    color: white; 
}
.text-large {
    font-family: SourceSans,Arial,sans-serif;
    font-size: 64px;
    font-style: normal;
    font-variant: normal;
    font-weight: bolder;
    line-height: 70px;
}
/* make sidebar nav vertical */ 
@media (min-width: 768px) {
  .sidebar-nav .navbar .navbar-collapse {
    padding: 0;
    max-height: none;
  }
  .sidebar-nav .navbar ul {
    float: none;
  }
  .sidebar-nav .navbar ul:not {
    display: block;
  }
  .sidebar-nav .navbar li {
    float: none;
    display: block;
  }
  .sidebar-nav .navbar li a {
    padding-top: 12px;
    padding-bottom: 12px;
  }
}
.circular--square {
  border-radius: 50%;
}
h3 {
    margin-top: 0px !important;
}
.login_banner_image {
    width: 400px;
}
#fileupload {
    padding-top: 20px;
}
.start, .cancel,.delete {
    border-radius: 5px;
    font-weight: bold;
    padding: 10px;
}
.btn.btn-success.fileinput-button.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-icon-primary {
    padding: 8px;
}
.col-lg-7 {
    width: auto;
}
.progress-extended {
    color: #ff0000 !important;
    font-size: 12px;
    height: 20px;
}
.progress {
    background-color: transparent !important ;
    border-radius: 4px;
    box-shadow: 0 0px 0px rgba(0, 0, 0, 0.1) inset !important;
    height: 20px;
    margin-bottom: 10px;
    overflow: hidden;
}
strong {
    color: red;
}
.progress.ui-progressbar.ui-widget.ui-widget-content.ui-corner-all {
    background: #808080 none repeat scroll 0 0 !important;
}
.col-md-2 > button {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0 !important;
    color: #000;
    font-size: 40px;
    text-transform: uppercase;
}
.chooseoption {
    background: #93e7e7 none repeat scroll 0 0;
 }
ul {
    font-size: 14px;
    padding-top: 0px !important;
}
.navicons
{
    width: 40px;
}
.nav.navbar-nav.navbar-right > li {
    padding-left: 20px;
}
.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {
    background-color: #00D9DB !important;
}
.navicons.small {
    width: 25px;
}
.divviy.feed.col6 {
    height: 700px;
    margin: auto;
    overflow: scroll;
    width: 50%;
}
.mainAdvert {
    width: 55%;
}
.col-sm-12 {
    text-align : center;
}
</style>
</head>
<body>

@include('includes.navbar')
<div class="panel-body">

@include('includes.uploadnav')

<div class="col-sm-6 col-md-6 col-lg-9">
