    </div>
  <br><br><br>
  </div>
</div>

  <div class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
      <p class="navbar-text pull-left">© 2016 DIVVIY      </p>
      
      <p class="navbar-text pull-right"><a target="_blank" href="https://www.facebook.com/divviy.app.1?fref=ts"><i class="fa fa-facebook fa-2x"></i></a></p>
      <p class="navbar-text pull-right"><a target="_blank" href="http://www.twitter.com/divviy"><i class="fa fa-twitter fa-2x"></i></a></p>
      <p class="navbar-text pull-right"><a target="_blank" href="http://www.instagram.com/divviy"><i class="fa fa-instagram fa-2x"></i></a></p>
      <p class="navbar-text pull-right"><a name="action" class="push-tagged ctaLink" href="#myModal" role="button" data-toggle="modal">CONTACT US</a></p>
    </div>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel">We'd Love to Hear From You</h4>
      </div>
      <div class="modal-body">
        <h3 style="color:#00d9db"><div class="success-alert"></div></h3>
        <form class="form-horizontal col-sm-12" id="contact-form">
          <div class="form-group">
            <label class="pull-left">* Name</label>
            <input name ="name" class="form-control required" placeholder="Your name" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text" required>
          </div>
          <div class="form-group">
            <label class="pull-left">* Subject</label>
            <input name ="subject" class="form-control required" placeholder="Your query" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text" required>
          </div>
          <div class="form-group">
            <label class="pull-left">* Message</label>
            <textarea id="messagecontent" class="form-control" placeholder="Your message here.." data-placement="top" data-trigger="manual" required></textarea></div>
          <div class="form-group">
            <label class="pull-left">* E-Mail</label>
            <input name ="email" class="form-control email" placeholder="email@you.com" data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@gmail.com)" type="text" required></div>
          <div class="form-group">
            <button type="submit" class="push-tagged buttonLink">Send It!</button> <p class="help-block pull-left text-danger hide" id="form-error">&nbsp; The form is not valid. </p></div>
        </form>
      </div>
      <div class="modal-footer">
        <!-- <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> -->
      </div>
    </div>
  </div>
</div>

<!-- AUDIO PLAYER DIV -->
<div class="foot" style="display:no">
<div class="col-md-10 divviy">
  <div class="audio-player">
    <h1 id="fileName">No Name</h1>
    <h2 id="authorName">No Author</h2>
    <h3 id="albumName">No Album</h3>
    <img class="cover" id="cover" src="/img/cover.png" alt="">
    <audio id="audio-player" src="http://www.noiseaddicts.com/samples_1w72b820/2558.mp3" type="audio/mp3" controls="controls"></audio>
  </div>
</div>
<div class="col-md-2" align="right">
<button id="pause">x</button>

</div>
</div>
<!-- SCRIPTS -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script> 


<!-- <script type="text/javascript">
jQuery(document).ready(function($) {
    $('#myCarousel').carousel({
      interval: 10000
  })
});
</script> -->
<!-- <script type="text/javascript">
jQuery(document).ready(function($) {
    $('#myCarousel1').carousel({
      interval: 10000
  })
});
</script> -->
<!-- DATE PICKER FORMAT -->
 <script type="text/javascript">
jQuery(document).ready(function($) {
               $("#datepicker").datepicker({ changeMonth: true,changeYear: true,yearRange : '-80:-13',dateFormat: "dd-mm-yy" }).val();
});
</script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
               $("#expirydate").datepicker({ changeMonth: true,changeYear: true,yearRange : '-0:+10',dateFormat: "mm/y" }).val();
});
</script>
<!-- COMMON CSRF TOKEN FOR ALL THE FORM -->
<script type="text/javascript">
$.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>
<!-- DATE PICKER FOR THE DATE -->
<script type="text/javascript">
    jQuery(document).ready(function($) {
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
</script>
<script>
  			jQuery(document).ready(function($) {
    		$( "#birthdate" ).datepicker();
  		});
</script>
<!-- RELOAD THE FORM TO CLEAR THANK YOU MESSAGE -->
<script type="text/javascript">    
    $(document).on("action", ".push-tagged.ctaLink", function () 
      {                
        document.getElementById("contact-form").reset();
        console.log('clicked');
        $('.success-alert').html("");
      });
</script>
<script type="text/javascript">
// POST VALUE TO postCHANGE METHOD IN ADMINCONTROLLER2
    $(document).on("click", ".push-tagged.buttonLink", function (e) 
      {
        e.preventDefault();
        if($('input[name=name]').val() == '' || $('input[name=subject]').val() == '' || $('input[name=messagecontent]').val() == '' || $('input[name=email]').val() == ''){
            alert('Fill all required fields');
        }
        var messagecontent = $('#messagecontent').val();

        $.ajax({
          type: "POST",
          url: "/contact/email",
          data: {'name':$('input[name=name]').val(),'subject':$('input[name=subject]').val(),'messagecontent':messagecontent,'email':$('input[name=email]').val(), '_token': $('input[name=_token]').val()},dataType: 'json',
          success: function(data){   
              console.log(data);
          },
          error: function(xhr, status, error) {
            console.log(status);
          }           
        });
          document.getElementById("contact-form").reset();
         $('.success-alert').html("Thank you ! We will get back to you soon.");
      });
</script>

<!-- DISPLAY USER PHOTO AND USER BILL FIELDS NAME -->
<script type="text/javascript">
 jQuery(document).ready(function($) {
    document.getElementById("user_photo").onchange = function () {
      var user_photo_file = this.value.split('\\').pop();
    document.getElementById("user_photo_field").value = user_photo_file;
  };
});
 jQuery(document).ready(function($) {
    document.getElementById("user_bill").onchange = function () {
    var user_bill_file = this.value.split('\\').pop();
    document.getElementById("user_bill_field").value = user_bill_file;
  };
});
</script> 
<!-- ACCEPT TERMS AND CONDITIONS-->
<script type="text/javascript">
 jQuery(document).ready(function($) {
  function checkForm(form)
  {
    if(!form.terms.checked) {
      alert("Please indicate that you accept the Terms and Conditions.");
      form.terms.focus();
      return false;
    }
    return true;
  }
});
</script>

<!-- PASSWORD CONFIRMATION -->
<script type="text/javascript">
 if ($("#password").val() != $("#password_confirmation").val()) {
          alert("Passwords do not match.");
      }
</script>

 <!-- SWITCH OFF AUTO COMPLETE  -->
<script type="text/javascript">
$('input').attr('autocomplete','off');
</script>
<script type="text/javascript">
if(!form.checkme.checked) {
      alert("Please indicate that you accept the Terms and Conditions.");
      form.checkme.focus();
      return false;
    }
    return true;
</script>
<!-- ONLY RETURN NUMBERS -->
<script type="text/javascript">
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
<!-- POST CODE SEARCH -->
<script type="text/javascript">

jQuery(document).ready(function($) {
        $("#postCodeBtn").click( function(e)
           {
            $('#postCodeSearch').hide();
            $('#postCodeRefresh').show();
             var postcode  = document.getElementById("zipcode").value;
             if(postcode == "")
             {
                alert("Enter Valid post code");
                $('#postCodeSearch').show();
                $('#postCodeRefresh').hide();

             }
          $.ajax({
            type: "POST",
            dataType:"json",
            url: "/postcode/"+ postcode,
            success: function(data){    
              document.getElementById("line2").value = data.street;
              document.getElementById("town_city").value = data.town + ", " + data.county;
              // document.getElementById("country").value = data.country.toUpperCase();
              $('#postCodeSearch').show();
              $('#postCodeRefresh').hide();

            },
            error: function(xhr, status, error) {
            console.log(status);
            }           
            });
             console.log(postcode);
           }
        );
  });
</script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="{{ asset('js/mediaelement-and-player.min.js') }}"></script>

<!-- AUDIO PLAYER -->
<script type="text/javascript">
function selvu(audioUrl,coverUrl,albumName,authorName,fileName){
  if($('.foot').hasClass('slide-up')) {
     $('.foot').removeClass('slide-down');
        $('.foot').addClass('slide-up', 2000, 'easeInBounce'); 
        console.log('playing the audio .....');        
          $(document).ready(function(){
              $(".edaaa").click(function(){
                 $('video,audio').each(function() {
                $(this)[0].stop();
              });
        
              $('video,audio').each(function() {
        
        document.getElementById("authorName").innerHTML = authorName;
        document.getElementById("albumName").innerHTML = albumName;
        document.getElementById("fileName").innerHTML = fileName;
        console.log('slide-up');
        $("#cover").attr("src",coverUrl);
        $('.foot').removeClass('slide-down');
        $('.foot').addClass('slide-up', 2000, 'easeInBounce');
                $(this)[0].setSrc(audioUrl);
                $(this)[0].play();
              });
            });
          });
      } 
      else {
        document.getElementById("authorName").innerHTML = authorName;
        document.getElementById("albumName").innerHTML = albumName;
        document.getElementById("fileName").innerHTML = fileName;
        $("#cover").attr("src",coverUrl);
        $('.foot').removeClass('slide-down');
        $('.foot').addClass('slide-up', 2000, 'easeInBounce'); 
      }
}

$("#audio-player").mediaelementplayer({
    // API options
    pluginVars: 'isvideo=true',
    enablePluginDebug: true,
            plugins: ['flash','silverlight']
});
$(document).ready(function(){

    $("#pause").click(function(){
        $('video,audio').each(function() {
      $(this)[0].pause();
  });
        $('.foot').addClass('slide-down', 500, 'easeOutBounce');
        $('.foot').removeClass('slide-up'); 
        return false;
  });
  console.log('stop audio');
});
$(document).ready(function(){
    $(".edaaa").click(function(){
       var audioUrl  = $(this).data('audiourl');
      console.log('first name');
        $('video,audio').each(function() {
          $(this)[0].setSrc(audioUrl);
      $(this)[0].play();
    });
  });
});
</script>
<!-- NAVIGATION BAR ACTIVE STATE -->
<script type="text/javascript">
jQuery(document).ready(function($) {
  var current_page_URL = location.href;
  $( "a" ).each(function() {
     if ($(this).attr("href") !== "#") {
       var target_URL = $(this).prop("href");
       if (target_URL == current_page_URL) {
          $('nav a').parents('li, ul').removeClass('active');
          $(this).parent('li').addClass('active');
          return false;
       }
     }
  });
});
</script> 
<!-- TAB BAR NAVIGATION -->
<script type="text/javascript">
 jQuery(document).ready(function($) {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });

});
</script>
</body>
</html>