@extends('includes.defaultupload')
@section('content')
<?php echo(stepupload('BOOK',2)) ?>

{!! Form::model($albumDetails,['class'=>"form-horizontal",'id'=>"fileupload",'method'=>'POST', 'files'=>true]) !!}

<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
       <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="icon-plus icon-white"></i>
                    <span>ADD FILES</span>
                    <input type="file" name="files[]" multiple accept=".epub,.pdf,.mobi">
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="icon-upload icon-white"></i>
                    <span>upload All</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="icon-ban-circle icon-white"></i>
                    <span>Cancel</span>
                </button>
                <div class="col-md-2 pull-right">
                        <input id ="nextButton" type="button" onclick="location.href='{{ url('/upload/books/3') }}';" class="btn btn-danger start ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" value="NEXT" style="display:none">
                </div>
                                <!-- The global progress bar -->
                <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped"></div>
                </div>
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-4 fileupload-progress">
                <!-- The global progress bar -->
<!--                 <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
 -->                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
{!! Form::close() !!}

<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
</div>
</DIV>
 <div class="row">
      <div class="col-sm-12">
        <img src={{asset('images/book_stages.png')}} alt="main advert" class="mainAdvert">
      </div>
 </div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error"></strong>
        </td>
        <td>
            <p class="size">100%</p>
            
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" disabled title="Upload">Upload</button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" title="cancel">Cancel</i></button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
            </p>
            {% if (file.error) { %}
                <div><span class="error">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">100 %</span>
        </td>
        <td>
            <button class="delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>Delete</button>
            <input type="checkbox" name="delete" value="1" class="toggle">
        </td>
    </tr>
{% } %}
</script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads {{ asset('js/main.js') }} -->
<script src="{{ asset('js/jquery.iframe-transport.js') }}"></script>
<!-- The basic File Upload plugin -->
<script src="{{ asset('js/jquery.fileupload.js') }}"></script>
<!-- The File Upload processing plugin -->
<script src="{{ asset('js/jquery.fileupload-process.js') }}"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="{{ asset('js/jquery.fileupload-image.js') }}"></script>
<!-- The File Upload audio preview plugin -->
<script src="{{ asset('js/jquery.fileupload-audio.js') }}"></script>
<!-- The File Upload validation plugin -->
<script src="{{ asset('js/jquery.fileupload-validate.js') }}"></script>
<!-- The File Upload user interface plugin -->
<script src="{{ asset('js/jquery.fileupload-ui.js') }}"></script>
<!-- The File Upload jQuery UI plugin -->
<script src="{{ asset('js/jquery.fileupload-jquery-ui.js') }}"></script>
<!-- The main application script -->
<script src="{{ asset('js/main.js') }}"></script>

<script type='text/javascript'>

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({

        // xhrFields: {withCredentials: true},
        dataType: 'json',
        url: '/upload/books/2',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(epub|pdf|mobi)$/i,
        maxFileSize: 350000000, //350MB
        maxNumberOfFiles: 1,
        progress: function (e, data) {
            var file = data.files[0];
            var progress = parseInt(data.loaded / data.total * 100, 10);

            $('#pgr-'+file.name).css(
                'width',
                50 + '%'
            );
        }

        }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    });
    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
    
    $('#fileupload').bind('fileuploadprogress', function (e, data) {
    // Log the current bitrate for this upload:
    console.log(data.bitrate);
});
    var $fileInput = $('#fileupload');

    $fileInput.on('fileuploaddone', function(e, data) {
        var activeUploads = $fileInput.fileupload('active');
        /*
         * activeUploads starts from the max number of files you are uploading to 1 when the last file has been uploaded.
         * All you have to do is doing a test on it value.
         */
        if(activeUploads == 1) {
            console.info("All uploads done");
               document.getElementById('nextButton').style.display = 'block';
        }
    });
});
</script>
@endsection
