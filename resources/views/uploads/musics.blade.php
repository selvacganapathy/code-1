@extends('includes.default')
@section('content')
<div class="panel-body">
	<div class="col-sm-6 col-md-6 col-lg-3">
	  <div class="sidebar-nav">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
          <ul class="nav navbar-nav">
          	<li><a href="/upload">CHOOSE OPTION <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></li>
            <li class="active"><a href="/upload/musics">MUSICS</a></li>
            <li><a href="/upload/books/1">BOOKS</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
	</div>
	<div class="col-sm-6 col-md-6 col-lg-9">
		
              {!! Form::open(array('action' => 'uploadController@musicsCreate','method'=>'POST' ,'files'=>true, 'class' => 'form-horizontal')) !!}
				<div class="form-group">
					<label class="col-md-4 control-label"> * Choose a file to upload: </label>
					<div class="fileUpload btn btn-primary">
			    		<span>CHOOSE FILE</span>
			    		<input id="upload_music" type="file" class="upload" name="upload_music[]" multiple/>
					</div>							
						<!-- <input id="upload_music" placeholder="Choose File" disabled="disabled" /> -->
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> * Choose a artwork: </label>
					<div class="fileUpload btn btn-primary">
			    		<span>CHOOSE FILE</span>
			    		<input type="file" class="upload" name="upload_artwork" accept=".png, .jpg, .jpeg"/>
					</div>							
						<!-- <input id="upload_artwork" placeholder="Choose File" disabled="disabled" /> </label> -->
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> * Choose a sample track: </label>
					<div class="fileUpload btn btn-primary">
			    		<span>CHOOSE File</span>
			    		<input  id="upload_sample" type="file" class="upload" name="upload_sample[]" multiple/>
					</div>							
<!-- 						<input id="upload_sample" placeholder="Choose File" disabled="disabled" />
 -->				</div><br>
				<div class="form-group">
					<label class="col-md-2 control-label"> * Artist Name: </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="artist_author_name" value="{{ old('artist_author_name') }}">
					</div>
					<label class="col-md-2 control-label"> * Genre: </label>
					<div class="col-md-4">
						{!! Form::select('category_genre_ID', $genre,['class'=>'form-control']) !!}
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label"> * Album Name: </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="album_title_name" value="{{ old('album_title_name') }}">
					</div>
				</div>
				<div class="form-group">	
					<div class="col-md-2">
						<button type="submit" class="btn btn-primary" style="width:130px;"><span class="glyphicon glyphicon-cloud" style="font-size: 1.2em;" aria-hidden="true"></button>
					</div>
					<div class="col-md-6">
						<italic>By uploading the files(left),you agree to our terms of service and privacy policy. Infringement of these
							will result in removal of your files</italic>
					<div class="checkbox">
					  <label><input type="checkbox" value="">Tick the box to confirm content belongs to you</label>
					</div>		
					</div>
				</div>
		</form>
	</div>
</div>
@endsection