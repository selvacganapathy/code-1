@extends('includes.default')
@section('content')
<div class="panel-body">
	<div class="col-sm-6 col-md-6 col-lg-3">
	  <div class="sidebar-nav">
      <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
      </div>
    </div>
	</div>
	<div class="col-sm-6 col-md-6 col-lg-9">
	<ul class='nav nav-wizard'>
  
  <li class='active'>Step 1 -  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Album</li>

  <li >Step 2 -  <span class="glyphicon glyphicon-open" aria-hidden="true"></span> Books</li>

  <li>Step 2 -  <span class="glyphicon glyphicon-open" aria-hidden="true"></span> Sample</li>

</ul>
		
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}" enctype="multipart/form-data">
				<div class="form-group">
					<label class="col-md-4 control-label"> * Choose a file to upload: </label>
					<div class="fileUpload btn btn-primary">
			    		<span>CHOOSE FILE</span>
			    		<input type="file" class="upload" name="user_bill" accept="application/pdf"/>
					</div>							
						<input id="uploadBill" placeholder="Choose File" disabled="disabled" />
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> * Choose a artwork: </label>
					<div class="fileUpload btn btn-primary">
			    		<span>CHOOSE FILE</span>
			    		<input type="file" class="upload" name="user_bill" accept="application/pdf"/>
					</div>							
						<input id="uploadBill" placeholder="Choose File" disabled="disabled" /> </label>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"> * Choose a sample track: </label>
					<div class="fileUpload btn btn-primary">
			    		<span>CHOOSE File</span>
			    		<input type="file" class="upload" name="user_bill" accept="application/pdf"/>
					</div>							
						<input id="uploadBill" placeholder="Choose File" disabled="disabled" />
				</div><br>
				<div class="form-group">
					<label class="col-md-2 control-label"> * Author Name: </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="town_city" value="{{ old('town_city') }}">
					</div>
					<label class="col-md-2 control-label"> * Category: </label>
					<div class="col-md-4">
						{!! Form::select('category', $categories) !!}
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label"> * Title Name: </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="zipcode" value="{{ old('zipcode') }}">
					</div>
				</div>
				<div class="form-group">	
					<div class="col-md-2">
						<button type="submit" class="btn btn-primary" style="width:130px;"><span class="glyphicon glyphicon-cloud" style="font-size: 1.2em;" aria-hidden="true"></button>
					</div>
					<div class="col-md-6">
						<italic>By uploading the files(left),you agree to our terms of service and privacy policy. Infringement of these
							will result in removal of your files</italic>
					<div class="checkbox">
					  <label><input type="checkbox" value="">Tick the box to confirm content belongs to you</label>
					</div>		
					</div>
				</div>
		</form>
	</div>
</div>
@endsection