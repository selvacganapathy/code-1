@extends('includes.default')
@section('content')
<div class="panel-body">
@include('includes.uploadnav')
<div class="col-sm-6 col-md-6 col-lg-9">
@if(Session::has('flash_message'))
    <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
@endif
@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

<?php echo(stepupload('BOOK',1)) ?>

{!! Form::model($albumDetails,['class'=>"form-horizontal"]) !!}
    		<div class="form-group">
					<label class="col-md-2 control-label"> * Author: </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="artist_author_name" value="{{ old('town_city') }}">
					</div>
					<label class="col-md-2 control-label"> * Title: </label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="album_title_name" value="{{ old('zipcode') }}">
					</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"> * Book Price (£):</label>
				<div class="col-md-4">
					<input type="text" class="form-control" name="album_price" value="{{ old('zipcode') }}">
				</div>
				<label class="col-md-2 control-label"> * Category: </label>
				<div class="col-md-4">
					{!! Form::select('category_genre_ID', $categories) !!}
				</div>
			</div>
			<div class="form-group">	
					<div class="col-md-2 pull-right">
						<button type="submit" class="btn btn-primary btn-lg">Next</button>
					</div>
			</div>
{!! Form::close() !!}
</div>
 <div class="row">
      <div class="col-sm-12">
        <img src={{asset('images/book_stages.png')}} alt="main advert" class="mainAdvert">
      </div>
 </div>
@endsection