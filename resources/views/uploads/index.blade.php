@extends('includes.default')
@section('content')
<div class="panel-body">
  <div class="col-sm-12">
    <h1>Instructions</h1>
  <ul>
      <li><b>All</b> media Must Be owned by the Individual uploading or the individual uploading must be authorised to do so by the owner</li>
      <li><b>For</b> music Uploads, Please Upload WAV and AIF Music Files Only. MP3 is not advised for Books uploads must be in PDF Format</li>
      <li><b>For</b> album and book art all images must be between 1200x1200 to 1500x1500 size</li>
      <li><b>All</b> songs uploaded must be labeled with the correct title</li>
  </ul>
    <div style="text-align:center;padding:10px;">
    <a class="btn btn-primary" href="{{ url('/upload/musics/1') }}">Upload Music</a>
    <a class="btn btn-primary" href="{{ url('/upload/books/1') }}">Upload books</a>
    </div>
    </div>
     <div class="row">
      <div class="col-sm-12">
        <img src={{asset('images/main_upload_image.png')}} alt="main advert" class="mainAdvert">
      </div>
    </div>
  </div>
</div>
</div>
@endsection