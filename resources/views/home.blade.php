@extends('includes.default')
@section('content')
<div class="panel-body">
	<div class="col-sm-6 col-md-6 col-lg-6">
		<img src={{asset('images/MAIN-PAGE-IMAGE3.jpg')}} alt="main advert" class="mainAdvertMain">
    </div>
	<div class="col-sm-6 col-md-6 col-lg-6">
	<P class="text-large" >FOLLOWING YOUR <span style="color:#00D9DB;">PEERS</span> INTEREST</P>
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src={{asset('images/feed_rotation_pic_1.jpg')}} alt="main advert" class="mainAdvert">
    </div>

    <div class="item">
      <img src={{asset('images/feed_rotation_pic_2.jpg')}} alt="main advert" class="mainAdvert">
    </div>

    <div class="item">
      <img src={{asset('images/feed_rotation_pic_3.jpg')}} alt="main advert" class="mainAdvert">
    </div>

    <div class="item">
      <img src={{asset('images/website_mockup.jpg')}} alt="main advert" class="mainAdvert">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
	<!-- <img src={{asset('images/website_mockup.jpg')}} alt="main advert" class="mainAdvert"> -->
		<div style="text-align:center;botton:0px;">
			<!-- <a href="/upload" ><img src={{asset('images/UPLOAD_BUTTON.png')}} alt="Logo"></a></div> -->
	</div>
</div>
@endsection	