@extends('includes.default')
@section('content')
<br><br>
<div class="container-fluid well span6">
  <div class="row-fluid">
   <div class="col-md-6">
        <div class="span2" >
    {!! Form::open(array('action' => 'feedController@changeProfilePic',Auth::user()->user_ID,'class'=>'form-horizontal-profile')) !!}
            {!! csrf_field() !!}
        <img width="140px" height="140px" src="{{ $profilePicUrl }}" class="img-circle"><br>
            <!-- <button type="submit" class="btn btn-danger">Change</button> -->

        </div>       
         {!! Form::close() !!}
    </div>  
     <div class="col-md-6">    
        <div class="span8">
            <h3>{{ ucfirst($userDetails->first_name) }} {{ ucfirst($userDetails->last_name) }}</h3>
            <h6>{{ ucfirst($userDetails->email_id) }}</h6>
            <h6>{{ ucfirst($userDetails->origanisation) }}</h6>
        </div>
     </div>   
        <div class="span2">
                <a class="btn btn-info" href="/edit/{{ ucfirst($userDetails->user_ID) }}">Edit Profile</a>
        </div>
</div>
</div>
<div class="panel panel-danger">
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#Music" aria-controls="Music" role="tab" data-toggle="tab">Music</a></li>
    <li role="presentation"><a href="#Books" aria-controls="Books" role="tab" data-toggle="tab">Books</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="Music">
<div class="divviy feed col6">
@if(!$urlMasterMainMusics->isEmpty())
@foreach($urlMasterMainMusics as $urlMasterMainDetails)
<div class="album">
 <h4><img width="140px" height="140px" src="{{ uploadprofilePicURl(Auth::user()->user_ID) }}
" class="img-circle-thumbnail">
   {{ ucfirst($userDetails->first_name) }} {{ ucfirst($userDetails->last_name) }}
   <h3>{{ ucfirst($urlMasterMainDetails->artist_author_name) }}</h3>
    <h5>{{ preg_replace('/\\.[^.\\s]{3,4}$/', '',$urlMasterMainDetails->filename) }} - {{ ucfirst($urlMasterMainDetails->album_title_name) }}<h4>  
    <hr class="divviycolor">
  <div class="albumCover">
    <?php $i=0; ?>
    <img class="img-responsive" src="{{ getArtwork($urlMasterMainDetails->upload_ID) }}" alt="" height="400">
    <div><a><img onmouseover="this.src='{{asset('images/play.png')}}'" onmouseout="this.src='{{asset('images/play_off.png')}}'" class="edaaa" src={{asset('images/play_off.png')}} 
      alt="" height="100%" 
      onclick="selvu('{{$urlMasterMainDetails->full_url}}','{{ getArtwork($urlMasterMainDetails->upload_ID) }}','{{ $urlMasterMainDetails->album_title_name }}','{{ $urlMasterMainDetails->artist_author_name }}','{{ preg_replace('/\\.[^.\\s]{3,4}$/', '',$urlMasterMainDetails->filename) }}')" 
      data-audioUrl="{{$urlMasterMainDetails->full_url}}"
      data-coverUrl=""
      data-albumName="{{ $urlMasterMainDetails->album_title_name }}"
    data-authorName="{{ $urlMasterMainDetails->artist_author_name }}"
      data-fileName="{{ preg_replace('/\\.[^.\\s]{3,4}$/', '',$urlMasterMainDetails->filename) }}"
      ></a></div>
    <?php $i++; ?>

</div>
</div>
@endforeach
@else
<h4 class="nofollowers">Follow accounts via the app to see music in your feed.</h4>
@endif

</div> 
    </div>
    <div role="tabpanel" class="tab-pane" id="Books">
      <div class="divviy feed col6">
@if(!$urlMasterMainBooks->isEmpty())
@foreach($urlMasterMainBooks as $urlMasterMainDetails)
<div class="album">
 <h4><img width="140px" height="140px" src="https://s3-eu-west-1.amazonaws.com/divviyaws/register/9/imagesttt.jpg" class="img-circle-thumbnail">
    {{ ucfirst($urlMasterMainDetails->artist_author_name) }}
    <h5>{{ preg_replace('/\\.[^.\\s]{3,4}$/', '',$urlMasterMainDetails->filename) }} - {{ ucfirst($urlMasterMainDetails->album_title_name) }}<h4>  
    <hr class="divviycolor">
  <div class="albumCover">
    <?php $i=0; ?>
    <img class="img-responsive" src="{{ getArtwork($urlMasterMainDetails->upload_ID) }}" alt="" height="400">
    <div><a href="{{$urlMasterMainDetails->full_url}}" target='_blank'><img onmouseover="this.src='{{asset('images/book_button_on.png')}}'" onmouseout="this.src='{{asset('images/book_button_off.png')}}'" class="edaaa" src={{asset('images/book_button_off.png')}}  alt="" height="100%"></a></div>
    <?php $i++; ?>

</div>
</div>
@endforeach
@else
<h4 class="nofollowers">Follow accounts via the app to see books in your feed</h4>
@endif
</div> 
    </div>
</div>
</div>
</div>
<div class="container">       
<div class="col-xs-12">
<div class="carousel slide" id="myCarousel">

<h3>My Uploads</h3>
        <div class="carousel-inner">
        @if(!$albums->isEmpty())
            {{ sliderCarousel(4,$albums) }}
        @else
            <h4 class="nofollowers">No Uploads</h4>
        @endif
        </div>
     <nav>
      <ul class="control-box pager">
        <li><a data-slide="prev" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-left"></i></a></li>
        <li><a data-slide="next" href="#myCarousel" class=""><i class="glyphicon glyphicon-chevron-right"></i></li>
      </ul>
    </nav>
     <!-- /.control-box -->   
                              
    </div><!-- /#myCarousel -->
</div>
</div>
@endsection
