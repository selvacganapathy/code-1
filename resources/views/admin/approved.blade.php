@extends('includes.default')

@section('content')
<h2>APPROVED MEMBERS</h2>

        <table class="table">
            <thead>
              <tr>
                <th>MEMBER ID</th>
                <th>FULL NAME</th>
                <th>EMAIL</th>
                <th>DOB</th>
                <th>GENDER</th>
                <th>ORG</th>
                <th>VIEW</th>
                <th>SINCE</th>
              </tr>
            </thead>
            <tbody>
              @foreach($userlist as $registeredUsers)
              <tr>
                  <td>{{ $registeredUsers->user_ID }}</td>
                  <td>{{ $registeredUsers->first_name }} {{ $registeredUsers->last_name }}</td>
                  <td>{{ $registeredUsers->email_id }}</td>
                  <td>{{ $registeredUsers->date_of_birth }}</td>
                  <td>{{ $registeredUsers->gender }}</td>
                  <td>{{ $registeredUsers->origanisation }}</td>
                  <td><a class="btn btn-primary" href="/admin/approved/{{ $registeredUsers->user_ID }}"><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span></a></td>
                  <td>{{ \Carbon\Carbon::parse($registeredUsers->updated_at)->diffForHumans() }}</td>
                  <td>
            		{!! Form::close() !!}
              </tr>
                @endforeach
            </tbody>  
          </table>
  {!! str_replace('/?', '?', $userlist->render()) !!}
@endsection
