@extends('includes.default')

@section('content')

<h2>MEMBERS</h2>
        <table class="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>EMAIL</th>
                <th>DOB</th>
                <th>GENDER</th>
                <th>ORG</th>
                <th>VERIFICATION</th>
                <th>AUTHORISE</th>
                <th>AT</th>
              </tr>
            </thead>
            <tbody>
              @foreach($userlist as $registeredUsers)
              <tr>
                  <td>{{ $registeredUsers->user_ID }}</td>
                  <td>{{ $registeredUsers->first_name }} {{ $registeredUsers->last_name }}</td>
                  <td>{{ $registeredUsers->email_id }}</td>
                  <td>{{ $registeredUsers->date_of_birth->format('d/m/Y') }}</td>
                  <td>{{ $registeredUsers->gender }}</td>
                  <td>{{ $registeredUsers->origanisation }}</td>
                  <td>
                    <a class="btn btn-primary" target="_blank" href="{{ asset('download/'.$registeredUsers->user_ID.'/photo/IMAGE_' .$registeredUsers->user_ID) }}"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></a>        	  
                    <a class="btn btn-primary" target="_blank" href="{{ asset('download/'.$registeredUsers->user_ID.'/utility/UTILITYBILL_' .$registeredUsers->user_ID) }}"><span class="glyphicon glyphicon-file" aria-hidden="true"></span></a>        	  
			            </td>
                    {!! Form::open(array('action' =>array('adminController@authorise', $registeredUsers->user_ID ))) !!}
                    {!! Form::hidden('email_id',$registeredUsers->email_id, array('id' => 'invisible_id')) !!}
                  <td>{!! Form::submit('AUTHORISE',array('class' => 'btn btn-primary','name' => 'action','id'=>'form')) !!}</td>
            		{!! Form::close() !!}
                <td>{{ \Carbon\Carbon::parse($registeredUsers->created_at)->diffForHumans() }}</td>
              </tr>
                @endforeach
            </tbody>  
          </table>
                    {!! str_replace('/?', '?', $userlist->render()) !!}

@endsection
