@extends('includes.default')

@section('content')
<h1 style="color:#00D9DB;">{{ ucfirst($uploadDetails->album_title_name) }} </h1>
<h3>By {{  ucfirst($uploadDetails->artist_author_name) }}</h3>
<h5>{{ $uploadDetails->option_name  }} - {{ $uploadDetails->category_genre_name }}</h5>
<h6>{{ $uploadDetails->created_at }}</h6>

  @foreach($bookDirectory as $booksMedia => $booksMediaValues)       
    <h3 style="text-transform:uppercase;color:#93E7E7;"> {{ $booksMedia }} </h3>
      <table class="table" style="width:90%">
          <tbody>
            @foreach($booksMediaValues as $Mediavalues)
            <tr>
              <td width="300px">{{ $Mediavalues }}</td>
              <td width="100px"><a class="btn btn-primary" href="
{{ asset('downloadMedia/'.$uploadDetails->option_name.'/'.$uploadDetails->uploaded_by.'/'.$uploadDetails->upload_ID.'/'.$booksMedia.'/'.$Mediavalues) }}
          ">download</a></td>
          @if($uploadDetails->option_name == "MUSICS" )
          <td width="100px">
            <audio controls>
              <source 
              src="{{ asset('downloadMedia/'.$uploadDetails->option_name.'/'.$uploadDetails->uploaded_by.'/'.$uploadDetails->upload_ID.'/'.$booksMedia.'/'.$Mediavalues) }}" 
              type="audio/*">
            </audio>
          </td>
          @endif
            </tr>
            @endforeach
          </tbody>  
      </table>
  @endforeach
@endsection

