@extends('includes.default')

@section('content')
<h1>Approve Media</h1>
<table class="table">
            <thead>
              <tr>
                <th>OPTION</th>
                <th>ALBUM NAME</th>
                <th>AUTHOR NAME</th>
                <th>UPLOADED AT</th>
                <th>VIEW</th>
                <th>AUTHORISE</th>
              </tr>
            </thead>
            <tbody>
              @foreach($userBookAlbums as $allbooks)
              <tr>
                  <td>{{ $allbooks->option_name }}</td>
                  <td>{{ $allbooks->album_title_name }}</td>
                  <td>{{ $allbooks->artist_author_name }}</td>
                  <td>{{ $allbooks->created_at }}</td>
                  <td><a class="btn btn-primary" href="/admin/approved/{{ $allbooks->option_name }}/{{ $allbooks->uploaded_by }}/{{ $allbooks->upload_ID }}">
                    VIEW</a></td>
                  @if($allbooks->is_active == 'N')
                  {!! Form::open(array('action' =>array('adminController@userMediaApprove', $allbooks->uploaded_by ))) !!}
                    {!! Form::hidden('option',$allbooks->option_name, array('id' => 'invisible_id')) !!}
                    {!! Form::hidden('upload_ID',$allbooks->upload_ID, array('id' => 'invisible_id')) !!}
                  <td>{!! Form::submit('AUTHORISE',array('class' => 'btn btn-primary','name' => 'action','id'=>'form')) !!}</td>
                  {!! Form::close() !!}
                  @else
                  <td><button class="btn btn-primary" disabled>AUTHORISED</a>
                  @endif

              </tr>
                @endforeach
            </tbody>  
          </table>

@endsection

