@extends('includes.default')

@section('content')
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center">
                  <h4 class="glyphicon glyphicon-edit"></h4><br/>Edit Profile
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="glyphicon glyphicon-lock"></h4><br/>Change Password
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="glyphicon glyphicon-modal-window"></h4><br/>Payment Details
                </a>
              </div>
            </div>
@if ($errors->has())
    <div class="paragraph" style="color:#990038;">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li> 
        @endforeach
    </div>
@endif            
@if(Session::has('flash_message'))
    <div class="alert alert-success"><em> {!! session('flash_message') !!}</em></div>
@endif
    @foreach($userDetails as $editprofile)
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- Edit section -->
                <div class="bhoechie-tab-content active">
        {!! Form::open(array('action' => 'editprofileController@editprofile',$editprofile->user_ID,'class'=>'form-horizontal')) !!}
            {!! csrf_field() !!}
                  <h1>EDIT PROFILE</h1>
                  <hr>
                  <div class="form-group">
                    {!! Form::label('name','First Name:  *',['class'=>"col-md-4 control-label"]) !!} 
                    <div class="col-md-6">     
                    {!! Form::text('first_name', $editprofile->first_name , ['class' => 'form-control'] ) !!}      
                    </div>
                  </div>
                  <div class="form-group">
                    {!! Form::label('name','Last Name:  *',['class'=>"col-md-4 control-label"]) !!} 
                    <div class="col-md-6">     
                    {!! Form::text('last_name', $editprofile->last_name , ['class' => 'form-control'] ) !!}      
                    </div>
                  </div>
                  <div class="form-group">
                    {!! Form::label('name','Phone Number:  *',['class'=>"col-md-4 control-label"]) !!} 
                    <div class="col-md-6">     
                    {!! Form::text('mobile_phone', $editprofile->mobile_phone , ['class' => 'form-control'] ) !!}      
                    </div>
                  </div>
                  {!! Form::hidden('user_ID', $editprofile->user_ID, array('id' => 'user_ID')) !!}
                  <div class="form-group" style="margin:50px;">  
                      <button type="submit" class="btn btn-primary">UPDATE</button>
                    </div>
                    <br><hr>
                  </div>
    {!! Form::close() !!}
                <!-- Password section -->
                <div class="bhoechie-tab-content">
    {!! Form::open(array('action' => 'editprofileController@editPassword',$editprofile->user_ID,'class'=>'form-horizontal')) !!}
            {!! csrf_field() !!}
                    <h1>CHANGE PASSWORD</h1>
                  <hr>
                  <div class="form-group">
                    {!! Form::label('name','Current Password:  *',['class'=>"col-md-4 control-label"]) !!} 
                    <div class="col-md-6">     
                    {!! Form::password('current_password', array('class' => 'form-control') ) !!}      
                    </div>
                  </div>
                  <div class="form-group">
                    {!! Form::label('name','New Password:  *',['class'=>"col-md-4 control-label"]) !!} 
                    <div class="col-md-6">     
                    {!! Form::text('new_password', null , ['class' => 'form-control'] ) !!}      
                    </div>
                  </div>
                  <div class="form-group">
                    {!! Form::label('name','Re Enter Password:  *',['class'=>"col-md-4 control-label"]) !!} 
                    <div class="col-md-6">     
                    {!! Form::text('new_password1', null , ['class' => 'form-control'] ) !!}      
                    </div>
                  </div>  
                  {!! Form::hidden('user_ID', $editprofile->user_ID, array('id' => 'user_ID')) !!}                
                  <div class="form-group" style="margin:50px;">  
                    <button type="submit" class="btn btn-primary">UPDATE</button>
                  </div>
                    <br><hr>
                </div>
    {!! Form::close() !!}
                <!-- Payment search -->
                <div class="bhoechie-tab-content">
    {!! Form::open(array('action' => 'editprofileController@editPayment',$editprofile->user_ID,'class'=>'form-horizontal')) !!}
    {!! csrf_field() !!}
                  <h1>PAYMENT DETAILS</h1>
                  <hr>
                  <div class="form-group">
                    {!! Form::label('name','Payment Type:  *',['class'=>"col-md-4 control-label"]) !!} 
                    <div class="col-md-6">     
                    {!! Form::select('payment_options_id', ['1' => 'PAYPAL']) !!}  
                    </div>
                  </div>
                  <div class="form-group">
                    {!! Form::label('name','Paypal Email ID:  *',['class'=>"col-md-4 control-label"]) !!} 
                    <div class="col-md-6">     
                    {!! Form::text('email', null , ['class' => 'form-control'] ) !!}      
                    </div>
                  </div>
                  {!! Form::hidden('user_ID', $editprofile->user_ID, array('id' => 'user_ID')) !!}                  
                  <div class="form-group" style="margin:50px;">  
                      <button type="submit" class="btn btn-primary">UPDATE</button>
                  </div>
                    <br><hr>
                </div>
            </div>   
            <hr>
        {!! Form::close() !!}
      <hr>
    
@endforeach
@endsection