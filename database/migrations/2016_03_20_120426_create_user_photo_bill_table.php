<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPhotoBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_photo_bill', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('upload_ID');
            $table->unsignedInteger('user_ID');
            $table->string('utility_url');
            $table->string('photo_url');
            $table->enum('is_active',['Y','N']);
            $table->timestamps();
            /**
             * primary - upload_ID
             */
            $table->foreign('user_ID')->references('user_ID')->on('user_master');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_photo_bill');
    }
}
