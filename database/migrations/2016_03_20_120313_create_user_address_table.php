<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_address', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('address_ID');
            $table->unsignedInteger('user_ID');
            $table->string('line1');
            $table->string('line2');
            $table->string('town_city');
            $table->string('country');
            $table->string('zipcode');
            $table->string('home_phone');
            $table->string('mobile_phone');
            $table->enum('is_active',['Y','N']);
            $table->timestamps();
            /**
             * primary - address_ID
             * index - address_ID
             * foreign - user_ID
             */
            $table->foreign('user_ID')->references('user_ID')->on('user_master');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_address');
    }
}
