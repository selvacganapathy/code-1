<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_cc', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('user_cc_ID');
            $table->unsignedInteger('user_ID');
            $table->unsignedInteger('payment_options_id');
            $table->string('card_name');
            $table->string('card_number');
            $table->integer('security_code');
            $table->string('sort_code');
            $table->string('account_number');
            $table->string('expiry_date');
            $table->string('email');
            $table->enum('is_active',['Y','N']);
            $table->timestamps();
            /**
             * primary - user_cc_ID
             * index - user_cc_ID,user_ID
             * foreign - user_ID
             */
            $table->foreign('user_ID')->references('user_ID')->on('user_master');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('user_cc');
    }
}
