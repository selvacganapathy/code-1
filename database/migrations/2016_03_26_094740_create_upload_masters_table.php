<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_masters', function (Blueprint $table) {
            $table->increments('upload_ID');
            $table->unsignedInteger('option_ID');                        
            $table->unsignedInteger('category_genre_ID');                        
            $table->integer('uploaded_by');                        
            $table->string('artist_author_name');                        
            $table->string('album_title_name');                        
            $table->decimal('album_price',5,2);
            $table->string('track_name');                        
            $table->enum('is_active',['Y','N']);
            $table->timestamps();

            $table->foreign('option_ID')->references('option_ID')->on('options_masters');
            $table->foreign('category_genre_ID')->references('category_genre_ID')->on('categories_genre');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('upload_masters');
    }
}
