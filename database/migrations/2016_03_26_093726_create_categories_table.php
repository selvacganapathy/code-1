<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_genre', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('category_genre_ID');
            $table->string('category_genre_name');            
            $table->unsignedInteger('option_ID');            
            $table->enum('is_active',['Y','N']);
            $table->timestamps();

            $table->foreign('option_ID')->references('option_ID')->on('options_masters');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories_genre');
    }
}
