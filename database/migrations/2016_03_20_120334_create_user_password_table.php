<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPasswordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_password', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('pwd_ID');
            $table->string('email_id')->unique();
            $table->string('remember_token');            
            $table->integer('user_ID');
            $table->string('password');
            $table->enum('user_type',['ADMIN','GENERAL','CLIENT']);
            $table->enum('is_active',['Y','N']);
            $table->timestamps();
            /**
             * primary - pwd_ID
             */
            $table->foreign('email_id')->references('email_id')->on('user_master');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_password');
    }
}
