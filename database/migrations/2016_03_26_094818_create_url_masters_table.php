<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrlMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('url_masters', function (Blueprint $table) {
            $table->increments('url_ID');
            $table->string('url_params');
            $table->string('full_url');
            $table->string('filename');
            $table->unsignedInteger('upload_ID');
            $table->timestamps();

            $table->foreign('upload_ID')->references('upload_ID')->on('upload_masters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('url_masters');
    }
}
