<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCloudUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cloud_urls', function (Blueprint $table) {
            $table->increments('cloud_ID');
            $table->string('base_url');
            $table->enum('cloud_type',['DEVELOPMENT','LIVE']);           
            $table->enum('is_active',['Y','N']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cloud_urls');
    }
}
