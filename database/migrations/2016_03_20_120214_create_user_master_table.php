<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('user_master', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('user_ID');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email_id')->unique();
            $table->date('date_of_birth');
            $table->enum('gender',['MALE','FEMALE']);
            $table->string('origanisation');
            $table->enum('is_active',['Y','N']);
            $table->timestamps();
            /**
             * primary - user_ID
             * indexes - 
             */

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_master');
    }
}
