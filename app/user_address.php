<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_address extends Model
{
    protected $primaryKey = 'address_ID';
    
    protected $table = 'user_address';   

    protected $fillable = [
    	'user_ID',
    	'line1',
    	'line2',
    	'town_city',
    	'country',
    	'zipcode',
    	'home_phone',
    	'mobile_phone',
    	'is_active'
    ];
}
