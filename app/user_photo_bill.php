<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_photo_bill extends Model
{
    protected $primaryKey = 'upload_ID';
    
    protected $table = 'user_photo_bill';   

    protected $fillable = [
    	'user_ID',
    	'utility_url',
    	'photo_url',
    	'is_active'
    ];
}
