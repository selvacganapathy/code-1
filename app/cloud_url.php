<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cloud_url extends Model
{
    protected $primaryKey = 'cloud_ID';
    
    protected $table = 'cloud_urls';   

    protected $fillable = [
    	'base_url',
    	'cloud_type',
    	'is_active',
    ];
}
