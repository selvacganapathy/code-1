<?php
use App\user_master;

function random_string() 
{
    $key = '';
    $keys = array_merge(range(0, 9), range('A', 'Z'),range('a', 'z'));

    for ($i = 0; $i < 32; $i++) {
        $key .= $keys[array_rand($keys)];
    }
    return time().'_'.$key;
}
function format_size($size) 
{
      $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
      if ($size == 0) { return('n/a'); } else {
      return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]); }
}
function image_size($filename)
{
	$imageSize['image_info'] = getimagesize($filename[0]->getRealPath().'/'.$filename[0]->getFilename());
	
	$imageSize['image_width'] = $image_info[0];
	
	$imageSize['image_height'] = $image_info[1];

	return $imageSize;
}

function file_extension($filename)
{
	$fileName['tmpFilename'] 	= $filename->getFilename(); //TEMP NAME
	$fileName['originalName'] 	= $filename->getClientOriginalName(); //ORIGINAL FILE NAME
	$fileName['fileSize']		= $filename->getClientSize(); //ACTUAL FILE SIZE
	$fileName['mimeType']		= $filename->getClientMimeType(); //MIME TYPE
	$fileName['fileExtension']  = $filename->guessClientExtension(); //FILE EXTENSION
	$fileName['filePath'] 		= $filename->getRealPath(); //FILE PATH

	$phpFileUploadErrors = array(
	    0 => 'There is no error, the file uploaded with success',
	    1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
	    2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
	    3 => 'The uploaded file was only partially uploaded',
	    4 => 'No file was uploaded',
	    6 => 'Missing a temporary folder',
	    7 => 'Failed to write file to disk.',
	    8 => 'A PHP extension stopped the file upload.',
	);
	
	if($fileName['fileSize'] > \Config::get('const.UTILITY_FILE_SIZE'))
	{
		return $phpFileUploadErrors[1].'size is'.format_size($fileName['fileSize']);
	}
	elseif($fileName['fileSize'] < 10 )
	{
		return $phpFileUploadErrors[4];
	}

	$ext = pathinfo($filename->getClientOriginalName(), PATHINFO_EXTENSION);

	$fileName['fullFilename'] = random_string().'.'.$ext;

	return $fileName;
}
 

function uploadAWSS3($fileArray,$options,$userID,$uploadID,$sample=null)
{
	$disk = Storage::disk('s3');

	if(is_null($sample))
	{
		$storageParam  = '/MAIN/';
	}
	else
	{
		$storageParam  = '/SAMPLE/';
	}
	foreach ($fileArray as $arrayKey => $arrayValue) 
        {
            $user_upload_sample = file_extension($arrayValue)['originalName'];

            $filePath = $options.$userID.'/'.$uploadID.$storageParam.$user_upload_sample;
        
            $disk->put($filePath,file_get_contents($arrayValue),'public');

        }

}
function uploadCover($file,$options,$userID,$uploadID)
{
	$disk = Storage::disk('s3');
	
	$user_upload_cover = file_extension($file)['originalName'];

    $filePath = $options.$userID.'/'.$uploadID.'/COVER/'.$user_upload_cover;

    $disk->put($filePath,file_get_contents($file),'public'); 
}

function stepupload($option,$step)
{

		if($option == 'BOOK')
		{
		$optionValue = 'Author';
		$optionStep2 = 'BOOK';		 
		}
		else
		{
		$optionValue = 'Artist';
		$optionStep2 = 'Album';

		}
        switch ($step)
        {
            case 1:
         $steps = "<ul class='nav nav-wizard'>
  					<li class='active'>Step 1 -  ".$optionValue."</li>
  					<li>Step 2 -   ".ucfirst(strtolower($optionStep2))."</li>
 					<li>Step 3 - Sample</li>
  					<li>Step 4 - Art Work</li>
					</ul>";
                break;
            case 2:
                $steps = "<ul class='nav nav-wizard'>
  					<li>Step 1 -  ".$optionValue."</li>
  					<li class='active'>Step 2 -   ".ucfirst(strtolower($optionStep2))."</li>
 					<li>Step 3 - Sample</li>
  					<li>Step 4 - Art Work</li>
					</ul>";
                break;
            case 3:
				$steps = "<ul class='nav nav-wizard'>
  					<li>Step 1 -  ".$optionValue."</li>
  					<li>Step 2 -   ".ucfirst(strtolower($optionStep2))."</li>
 					<li class='active'>Step 3 - Sample</li>
  					<li>Step 4 - Art Work</li>
					</ul>";
                break;
            case 4:
                $steps = "<ul class='nav nav-wizard'>
  					<li>Step 1 -  ".$optionValue."</li>
  					<li>Step 2 -   ".ucfirst(strtolower($optionStep2))."</li>
 					<li>Step 3 - Sample</li>
  					<li class='active'>Step 4 - Art Work</li>
					</ul>";
                break;

        }
        return $steps;
}
function getArtwork($uploadID)
{
	$artworkUrl = App\urlMaster::where('upload_ID',$uploadID)
								->where('url_params','artwork')
								->first();
	return $artworkUrl->full_url;
}

function getAlbumArtwork($uploadID)
{
	$artworkUrl = App\urlMaster::where('upload_ID',$uploadID)
								->where('url_params','artwork')
								->orderBy('url_ID','DESC')
								->take(1)
								->get(['full_url']);
	return $artworkUrl[0]->full_url;

}
function sliderCarousel($sliderCount,$elementArray)
{
	foreach ($elementArray->chunk(4) as $item) 
	{   
		if(isset($item[0])) 
		{
			$active = 'active';
		}
		else
		{
			$active = '';
		}

		echo "<div class='item $active'><ul class='thumbnails'>";	
		
		foreach($item as $kicks)
		{
			
    		echo "<li class='col-sm-3'>";
    		echo "<div class='fff'><div class='thumbnail'>";
    		echo "<a href='/feed/".$kicks->upload_ID."'><img src='".getArtwork($kicks->upload_ID)."' alt=''></a>"; //1
    		echo "</div><div class='caption'>";
    		echo "<a href='/feed/".$kicks->upload_ID."'><h4>Title - ".ucfirst($kicks->album_title_name)."</h4></a>"; //2
			echo "<p>Album - ".ucfirst($kicks->artist_author_name)."</p>"; //3
			echo "<p>Category - ".ucfirst($kicks->option_name)."</p>"; //3
    		echo "</li>";
		}
		echo '</ul></div>';
	}
}

function albumPortfolio($portfolioCount, $portfolioArray)
{

	foreach ($portfolioArray->chunk(3) as $item) 
	{ 

		echo "<div class='row'>";
		foreach ($item as $kicks) 
		{
			echo "<div class='col-md-4 portfolio-item'>";
			echo " <a href='#'>";
			echo "<div class='albumCover'>";
			echo "<img class='img-responsive' src='".getAlbumArtwork($kicks->upload_ID)."' alt=''>";
			if($kicks->option_ID == 1)
			{
                  	echo "<div><a><img src='../images/play.png' class='edaaa' alt='' width='10'";
                    echo "data-audioUrl='$kicks->full_url'";
                    echo "data-albumName='$kicks->album_title_name'";
                    echo "data-authorName='$kicks->artist_author_name'";
                	echo "data-fileName='$kicks->filename'";
                	echo "data-coverUrl=''";
                	echo 'onclick="selvu(';
                	echo "'$kicks->full_url',";
                	echo "'".getArtwork($kicks->upload_ID)."',";
                	echo "'$kicks->album_title_name',";
                	echo "'$kicks->artist_author_name',";
                	echo "'".preg_replace('/\\.[^.\\s]{3,4}$/', '',$kicks->filename)."'";
                	echo ')"';
                  	echo "id='play'></a></div>";
					echo "</a>";				
			}
			else
			{
				echo "<div><a href='$kicks->full_url' target='_blank'><img src='../images/book_button_on.png' class='edaaa' alt='' width='10' 
      				data-audioUrl='$kicks->full_url'
      				data-coverUrl='".getArtwork($kicks->upload_ID)."'
      				data-albumName='$kicks->album_title_name'
      				data-authorName='$kicks->artist_author_name'
      				onclick=selvu('$kicks->full_url','".getArtwork($kicks->upload_ID)."','$kicks->album_title_name','$kicks->artist_author_name','".preg_replace('/\\.[^.\\s]{3,4}$/', '',$kicks->filename)."')
      				id='play'></a></div>";
			echo "</a>";
			}

			echo "<h4>";
			echo $kicks->filename;
        	echo "</h4>";
			echo "</div></div>";
		}
		echo "</div>";
	}
}
function uploadprofilePicURl($userID)
{
	$userDetails = user_master::leftjoin('user_photo_bill','user_master.user_ID','=','user_photo_bill.user_ID')
                                  ->where('user_master.user_ID','=',$userID)
                                  ->first();
    if($userDetails)
    {
    	$profilePicUrl = \Config::get('const.AWS_STORAGE_URL').
                         \Config::get('const.AWS_STORAGE_URL_REGISTER').
                         $userID.                        
                         '/'.$userDetails->photo_url;
    }
	 $profilePicUrl = asset('images/no-image1.png');
    

    return $profilePicUrl;
}

?>	

