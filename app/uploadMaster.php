<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class uploadMaster extends Model
{
   	protected $primaryKey = 'upload_ID';
    
    protected $table = 'upload_masters';   

    protected $fillable = [
    	'option_ID',
    	'category_genre_ID',
    	'uploaded_by',
    	'artist_author_name',
    	'album_title_name',
        'album_price',
    	'track_name',
    	'is_active',
    ];
}
