<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use DB;
use App\uploadMaster;
use App\urlMaster;
use App\user_master;
use App\user_password;
use App\user_address;
use App\user_cc;
use App\Http\Requests;

class feedController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index()
    {

       $userDetails = user_master::leftjoin('user_photo_bill','user_master.user_ID','=','user_photo_bill.user_ID')
                                  ->where('user_master.user_ID','=',Auth::user()->user_ID)
                                  ->first();
        // //get artwork
        // $urlMasterArtwork = uploadMaster::leftjoin('url_masters','url_masters.upload_ID', '=','upload_masters.upload_ID')
        //                         ->where('upload_masters.uploaded_by',Auth::user()->user_ID)
        //                         ->where('url_masters.url_params','artwork')
        //                         ->where('upload_masters.is_active','Y')
        //                         ->get();

        //get sample music latest
        $urlMasterMainMusics = uploadMaster::leftjoin('url_masters','url_masters.upload_ID', '=','upload_masters.upload_ID')
                                ->where('upload_masters.is_active','Y')
                                ->where('url_masters.url_params','sample')
                                ->where('upload_masters.option_ID',1)
                                ->orderBy('upload_masters.option_ID','ASC')
                                ->take(\Config::get('const.MAX_DISPLAY_FEED_MUSIC'))
                                ->get(); 


        
        //get sample books latest
        $urlMasterMainBooks = uploadMaster::leftjoin('url_masters','url_masters.upload_ID', '=','upload_masters.upload_ID')
                                ->where('upload_masters.is_active','Y')
                                ->where('url_masters.url_params','sample')
                                ->where('upload_masters.option_ID',2)
                                ->orderBy('upload_masters.option_ID','ASC')
                                ->take(\Config::get('const.MAX_DISPLAY_FEED_BOOKS'))
                                ->get();                            
        
        // ->where('upload_masters.uploaded_by','!=',Auth::user()->user_ID)
        
        $albums = uploadMaster::leftjoin('options_masters','options_masters.option_ID','=','upload_masters.option_ID')
                                ->where('uploaded_by',Auth::user()->user_ID)
                                ->where('upload_masters.is_active','Y')
                                ->get();
        /**
         * compile the url to display profile pic
         */
        
        if($userDetails->photo_url)
        {
             $profilePicUrl = \Config::get('const.AWS_STORAGE_URL').
                         \Config::get('const.AWS_STORAGE_URL_REGISTER').
                         Auth::user()->user_ID.                        
                         '/'.$userDetails->photo_url;
        }
        else
        {
            $profilePicUrl = asset('images/no-image1.png');
        }
        
        return view('feed.index',compact('userDetails','profilePicUrl','urlMasterMainMusics','albums','urlMasterMainBooks'));
    }

    public function getAlbumList($upload_ID)
    {
        // $albums = urlMaster::where('upload_ID',$upload_ID)
        //                     ->where('url_params','main')
        //                     ->get();

        $albums = uploadMaster::leftjoin('url_masters','url_masters.upload_ID', '=','upload_masters.upload_ID')
                                ->where('upload_masters.upload_ID',$upload_ID)
                                ->where('upload_masters.is_active','Y')
                                ->where('url_masters.url_params','main')
                                ->get();
        // dd($albums);
        return view('feed.albumdetail',compact('albums'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $userDetails = DB::table('user_master')
                    ->select('user_password.*', 'user_master.*','user_cc.*','user_address.*')
                    ->leftjoin('user_password', 'user_master.user_ID', '=', 'user_password.user_ID') 
                    ->leftjoin('user_cc', 'user_master.user_ID', '=', 'user_cc.user_ID')
                    ->leftjoin('user_address', 'user_master.user_ID', '=', 'user_address.user_ID')
                    ->where(function($query) use ($id) {
                            $query->where('user_master.user_ID',$id);
                    })
                    ->get();
        
        return view('feed.editprofile',compact('userDetails'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
public function changeProfilePic()
{

    dd('Work in progress');
}
}
