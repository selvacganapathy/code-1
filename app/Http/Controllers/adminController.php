<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Mail;
use File;
use DB;
use Storage;
use App\cloud_url;
use App\user_cc;
use App\user_photo_bill;
use App\user_master;
use App\user_address;
use App\user_password;
use App\uploadMaster;
use App\urlMaster;
use App\Http\Requests;

class adminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        /**
         * GET THE USER LIST OF REGISTERED USERS
         */
    	$userlist = user_master::join('user_password','user_master.email_id','=','user_password.email_id')
					->where('user_password.user_type','=','CLIENT')
					->where('user_master.is_active','=','N')
    				->paginate(30);

    	return view('admin.index',compact('userlist'));
    }

    public function create()
    {
    	$action = 'post' . preg_replace('/\s+/', '', Input::get('action'));
        $this->$action();
    }

    public function authorise($id)
    {
        /**
         * LOCAL_STORAGE_URL_REGISTER - reference const.php 
         * s3 - live storage disk
         * $filename - get the actual filename from $files
         * $respondError - will respond error back to laravel from AWS (not registered in laravel log)
         */

        $files = File::files(storage_path(\Config::get('const.LOCAL_STORAGE_URL_REGISTER').$id));

        foreach($files as $file)
        {
            $disk = Storage::disk('s3');
            
            $filename = pathinfo($file)['basename'];

            $filePath = \Config::get('const.AWS_STORAGE_URL_REGISTER').$id.'/'.$filename;

            try 
            {
                $disk->put($filePath,file_get_contents($file),'public');
            } 
            catch (Exception $e) 
            {
                return $this->respondError($e->getMessage());
            }
            
            User_photo_bill::where('user_ID','=',$id)->update(['is_active' => 'Y']);

        }

        $success = File::deleteDirectory(storage_path(\Config::get('const.LOCAL_STORAGE_URL_REGISTER').$id));
        /**
         * approve user masters, card details, address and password
         * get and put card details has been deprecated after discussion with client on --/--/--
         */
        user_master::where('user_ID','=',$id)->update(['is_active' => 'Y']);
        user_address::where('user_ID','=',$id)->update(['is_active' => 'Y']);
    	user_password::where('user_ID','=',$id)->update(['is_active' => 'Y']);
    	
    	Mail::send('emails.success_user',Input::get(), function($message)
            {
                $message->from(\Config::get('const.NO_REPLY_EMAIL'), 'DIVVIY');
                $message->to(Input::get('email_id'))->subject('Divviy Authorisation Email');;
            });

        Mail::send([],Input::get(), function($message)
            {
                    $message->from(\Config::get('const.NO_REPLY_EMAIL'), 'DIVVIY');
                    $message->to(\Config::get('const.ADMIN_EMAIL'))->subject('AUTHORISED USER '.Input::get('email_id'));
                    $message->setBody('Hi All, the user  '.Input::get('email_id').'has been authorised at'.date('l jS \of F Y h:i:s A'));
            });
    	return $this->index();
    }
    /**
     * unauthorise is not implemented yet - not in scope
     */
    public function unauthorise($id)
    {
    	user_master::where('user_ID','=',$id)->update(['is_active' => 'N']);
    	
    	Mail::send('emails.success_user',Input::get(), function($message)
            {
                $message->from('no-reply@scubess.com', 'DS Franchising');
                $message->to(Input::get('email_id'))->subject('DIVVIY AUTHORISATION');;
            });

    	return $this->index();
    }
    /**
     * get the list of approved users 
     */
    public function approved()
    {
        $userlist = DB::table('user_master')
                    ->select('user_master.*','user_password.*','user_photo_bill.*')
                    ->join('user_password', 'user_master.email_id', '=', 'user_password.email_id') 
                    ->leftjoin('user_photo_bill', 'user_master.user_ID', '=', 'user_photo_bill.user_ID')
                    ->where('user_password.user_type','=','CLIENT')
                    ->where('user_master.is_active','=','Y')
                    ->orderBy('user_master.user_ID', 'desc')
                    ->paginate(30);
        
        return view('admin.approved',compact('userlist'));
    }
    /**
     * download the approved user photobill or utility bill
     * $id - get the user ID from the table
     * $option - get whether book or musics
     * $filename - get the filename to download 
     */
    public function download($id,$option,$filename){

        $user_photo_bill = User_photo_bill::where('user_ID','=',$id)->get();

            if($option !== 'photo')
            {
                $filename = $user_photo_bill[0]->utility_url;
            }
            else
            {
                $filename = $user_photo_bill[0]->photo_url;
            }
            
            $file = storage_path('uploads/register/'.$id) . '/' . $filename;

            return response()->download($file);

    }
    /**
     * list of user media in the user media page
     * get the list of user media albums by books / musics 
     * need to change to pagination
     */
    public function userMedia($id){

           $userBookAlbums = DB::table('upload_masters')
                    ->select('upload_masters.*','categories_genre.category_genre_name','options_masters.option_name')
                    ->leftjoin('categories_genre', 'upload_masters.category_genre_ID', '=', 'categories_genre.category_genre_ID')
                    ->leftjoin('options_masters', 'upload_masters.option_ID', '=', 'options_masters.option_ID')
                    ->where('upload_masters.uploaded_by','=',$id)
                    ->get();
        return view('admin.usermedia',compact('userBookAlbums'));
    }
    /**
     * approve user media and upload to S3
     * structure - 
     */
    public function userMediaApprove($id)
    {
        /**
         * MAIN FILE 
         */

        $mainLocalFilePath = storage_path().'/uploads/'
                        .strtolower(Input::get('option'))
                        .'/'.$id
                        .'/'.Input::get('upload_ID');


        $files = File::allFiles($mainLocalFilePath);

        foreach ($files as $filesKey => $filesValue) 
        {
            $disk = Storage::disk('s3');

            $mainUploadfilePath = '/'.strtolower(Input::get('option')).'/'.$id.'/'.Input::get('upload_ID');
            
            $filePath = $mainUploadfilePath.'/'.$filesValue->getRelativePath().'/'.$filesValue->getFilename();    
            
            try 
            {
                $disk->put($filePath,file_get_contents($mainLocalFilePath.'/'.$filesValue->getRelativePath().'/'.$filesValue->getFilename()),'public');
            
                $data['url_params']   =  $filesValue->getRelativePath();
                $data['full_url']     =  \Config::get('const.AWS_STORAGE_URL').$filePath;
                $data['upload_ID']    =  Input::get('upload_ID');
                $data['filename']     =  $filesValue->getFilename();

                urlMaster::create($data);

            } 
            catch (Exception $e) 
            {
                return $this->respondError($e->getMessage());
            }
        }

        // $success = File::deleteDirectory(storage_path().'/uploads/register/'.$id);
        uploadMaster::where('upload_ID','=',Input::get('upload_ID'))->update(['is_active' => 'Y']);
        
        return $this->userMedia($id);
    }
    /**
     * view all the uploaded files 
     */
    public function viewAlbum($option,$id,$upload_ID)
    {
        /**
         * GET UPLOAD DETAILS
         */        
        $uploadDetails = DB::table('upload_masters')
                    ->select('upload_masters.*','categories_genre.category_genre_name','options_masters.option_name')
                    ->leftjoin('categories_genre', 'upload_masters.category_genre_ID', '=', 'categories_genre.category_genre_ID')
                    ->leftjoin('options_masters', 'upload_masters.option_ID', '=', 'options_masters.option_ID')
                    ->where('upload_ID','=',$upload_ID)
                    ->first();

        $bookList = storage_path('uploads/'.strtolower($option).'/'.$id.'/'.$upload_ID) . '/';

        $books = File::allFiles($bookList);

        foreach ($books as $bookskey => $booksvalue) 
        {   
            $bookDirectory[$booksvalue->getRelativePath()][] =  $booksvalue->getFilename();
        }
        return view('admin.viewAlbum',compact('bookDirectory','uploadDetails'));
    }
    
    /**
     * download music album 
     */
    public function downloadAlbum($options,$id,$upload_ID,$filename,$option = null)
    {
        if($options == "MUSICS")
        {
            $storageURL = \Config::get('const.LOCAL_STORAGE_URL_MUSICS');
        }
        elseif($options == "BOOKS")
        {
            $storageURL = \Config::get('const.LOCAL_STORAGE_URL_BOOKS');
        }

        if(is_null($option))
        {
            $file = storage_path().$storageURL.$id.'/'.$upload_ID.'/'.$filename;
        }
        else
        {
            $file = storage_path().$storageURL.$id.'/'.$upload_ID.'/'.$filename.'/'.$option;
        }
        return response()->download($file);
    }

}