<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

use Request;
use Input;
use Mail;
class contactController extends Controller
{
    public function index()
    {       
    	if(Request::ajax()) 
        {
        	$input = Input::all();

            Mail::send('emails.thankyou_user',$input, function($message)
            {
                $message->from(\Config::get('const.NO_REPLY_EMAIL'), 'DIVVIY');
                $message->to(Input::get('email'))->subject('DIVVIY Enquiry Form');
            });
            
            if(count(Mail::failures()) > 0 ) 
            {
                dd(Mail::failures());
            }
       		Mail::send('emails.contactform_admin',$input, function($message)
        	{
            	$message->from(Input::get('email'), 'DIVVIY');
            	$message->to(\Config::get('const.CONTACT_SUPPORT_EMAIL'))->subject('DIVVIY Enquiry Form');
        	});
            
        }
    }
}
