<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\user_address;
use App\user_cc;
use Input;
use Redirect;
use App\user_master;
use App\user_password;
use App\user_photo_bill;
use Validator;
use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use Illuminate\Support\Facades\Storage;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $username = 'email_id';
    protected $redirectTo = '/home';
    protected $redirectPath = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name'            => 'required',
            'last_name'             => 'required',
            'origanisation'         => 'required',
            'line1'                 => 'required',
            'date_of_birth'         => 'required|date_format:d-m-Y',
            'gender'                => 'required',
            'email_id'              => 'required|email|confirmed',
            'email_id_confirmation' => 'required|same:email_id',
            'town_city'             => 'required',
            'country'               => 'required',
            'zipcode'               => 'required',
            'mobile_phone'          => 'required',
            'password'              => 'required',
            'password_confirmation' => 'required|same:password',
            'user_photo'            => 'required',
            'user_bill'             => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        
        if(is_null($this->existingUser(Input::get('email_id'))))
        {        
            $user_bill_name = file_extension($data['user_bill'])['fileExtension'];

            $user_photo_name  = file_extension($data['user_photo'])['fileExtension'];

            $data['password'] = bcrypt($data['password']);

            $data['date_of_birth'] =  date("Y-m-d", strtotime($data['date_of_birth']));
            
            $insertedId = user_master::create($data);
            
            $data['user_ID'] = $insertedId->user_ID;

            $uploadPath = storage_path().DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'register'.DIRECTORY_SEPARATOR.$data['user_ID'];

            $data['user_photo']->move($uploadPath, preg_replace("/\s+/", "", stripslashes($data['user_photo']->getClientOriginalName())));
            
            $data['user_bill']->move($uploadPath, preg_replace("/\s+/", "", stripslashes($data['user_bill']->getClientOriginalName())));

            $data['photo_url'] = preg_replace("/\s+/", "", stripslashes($data['user_photo']->getClientOriginalName())); //'IMAGE_'.$data['user_ID'].$user_photo_name;

            $data['utility_url'] =  preg_replace("/\s+/", "", stripslashes($data['user_bill']->getClientOriginalName())); //'UTILITYBILL_'.$data['user_ID'].$user_bill_name;
            
            // User_cc::create($data);
            
            user_address::create($data);
            
            $data['user_type'] = 'CLIENT';
            
            user_photo_bill::create($data);

            user_password::create($data);

            Mail::send('emails.thankyou_user_reg',$data, function($message) 
                {
                    $message->from(\Config::get('const.NO_REPLY_EMAIL'), 'DIVVIY');
                    $message->to(Input::get('email_id'))->subject('Divviy Authorisation Email');;
                });

            Mail::send([],$data, function($message) use($uploadPath,$data)
            {
                    $message->from(\Config::get('const.NO_REPLY_EMAIL'), 'DIVVIY');
                    $message->to(\Config::get('const.NEW_REGISTRATION_EMAIL'))->subject('NEW REGISTRATION from '.Input::get('email_id'));
                    $message->setBody('Hi Admin,<br> I am a new user. please Authorise me. '.Input::get('email_id'));                        
                    $message->attach($uploadPath.DIRECTORY_SEPARATOR.$data['photo_url']); 
                    $message->attach($uploadPath.DIRECTORY_SEPARATOR.$data['utility_url']); 
            });
        }
        else
        {
                return Redirect::to('auth.login')
                       ->withErrors([
                    'username' => 'You already registered your interest. Thank you ',
                ]);
        }
        
        return;
    }

    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) 
        {
           $this->throwValidationException(
               $request, $validator
           );
        }

        $this->create($request->all());

        return redirect($this->redirectPath());
    }

    protected function getCredentials(Request $request) {

        $request['is_active'] = 'Y';
        return $request->only($this->loginUsername(), 'password', 'is_active');
    }

    public function existingUser($emailID)
    {
        $checkExistingUser =  user_master::all()->where('email_id',$emailID);

        if($checkExistingUser->isEmpty())
        {
            return NULL;
        }
        else
        {
            return $checkExistingUser;
        }
    }

}
