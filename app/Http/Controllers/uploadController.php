<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use Input;
use Mail;
use Validator;
use Redirect;
use Session;
use App\categories_genre;
use App\uploadMaster;
use App\urlMaster;
use App\Http\Requests;

class uploadController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    protected $lastStep = 4;
    
    public function index()
    {
        return view('uploads.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getMusicsStep(Request $request, $step)
    {
        $categories = categories_genre::where('option_ID','=',1)->lists('category_genre_name', 'category_genre_ID');

        $option_ID = 1;

        return view('uploads.musics_step_'.$step, ['albumDetailsMusic' => $request->session()->get('albumDetails'),'categories'=>$categories,'option_ID'=>$option_ID]);
    }

    public function getBooksStep(Request $request, $step)
    {
        $categories = categories_genre::where('option_ID','=',2)->lists('category_genre_name', 'category_genre_ID');

        $option_ID = 2;

        return view('uploads.books_step_'.$step, ['albumDetails' => $request->session()->get('albumDetails'),'categories'=>$categories,'option_ID'=>$option_ID]);
    }
    public function postBooksStep(Request $request, $step)
    {
        $data['user_ID'] =  Auth::user()->user_ID;

        switch ($step)
        {
            case 1:
                $rules = ['artist_author_name' => 'required',
                            'album_title_name' => 'required',
                            'album_price'      => 'required'];
                break;
            case 2:
                $rules = [];
                break;
            case 3:
                $rules = [];
                break;
            case 4:
                $rules = [];
                break;

            default:
                abort(400, "No rules for this step!");
        }

        $validate = $this->validate($request, $rules);

        if ($validate) 
        {
           $this->throwValidationException(
               $request, $validator
           );
        }

        /**
         * CREATE ALBUM DETAILS
         * @var [type]
         */
        if($step == 1)
        {
            $data['category_genre_ID'] = $request->input('category_genre_ID');

            $data['album_title_name'] = $request->input('album_title_name');

            $data['artist_author_name'] = $request->input('artist_author_name');

            $data['album_price'] = $request->input('album_price');

            $data['option_ID'] = 2;

            $data['is_active'] = 'N';
            
            $data['uploaded_by'] = Auth::user()->user_ID;

            $insertedId =  uploadMaster::create($data);

            $data['upload_ID'] = $insertedId->upload_ID;

            $albumDetails = ['album_title_name'  => $request->input('album_title_name'),
                             'option_ID'         => $data['option_ID'],
                             'category_genre_ID' => $request->input('category_genre_ID'),
                             'submitted_by'      => Auth::user()->user_ID,
                             'artist_author_name'=> $request->input('artist_author_name'),
                             'upload_ID'         => $data['upload_ID']];

            $request->session()->put('albumDetails', $albumDetails);

        }
        /**
         * UPLOAD MUSIC FILES
         * @var [type]
         */
        if($step == 2)
        {

            // Grab our files input
            $files = Input::file('files');
            
            // We will store our uploads in public/uploads/basic
            
            // $assetPath = storage_path().'/uploads';
            $assetPath = storage_path().DIRECTORY_SEPARATOR.'uploads'
                                       .DIRECTORY_SEPARATOR.'books'
                                       .DIRECTORY_SEPARATOR.$data['user_ID']
                                       .DIRECTORY_SEPARATOR.$request->session()->get('albumDetails')['upload_ID']
                                       .DIRECTORY_SEPARATOR.'main';

            $uploadPath = $assetPath;
            
            // We need an empty arry for us to put the files back into
            $results = array();
            
            foreach ($files as $file) 
            {    
                // store our uploaded file in our uploads folder
                $file->move($uploadPath, $file->getClientOriginalName());
                
                // set our results to have our asset path
                $name = $file->getClientOriginalName();
                $results[] = compact('name');
            }
            
            // $request->session()->get('albumDetails')->update(Input::all());
            Session::push('albumDetails.track_name', $results);

            // return our results in a files object
            return array(
                'files' => $results
            );
            
        }

        
        /**
         * UPLOAD MUSIC SAMPLE
         * @var [type]
         */
        if($step == 3)
        {   
            // Grab our files input
            $files = Input::file('files');
            
            // We will store our uploads in public/uploads/basic
            
            // $assetPath = storage_path().'/uploads';
            $assetPath = storage_path().DIRECTORY_SEPARATOR.'uploads'
                                       .DIRECTORY_SEPARATOR.'books'
                                       .DIRECTORY_SEPARATOR.$data['user_ID']
                                       .DIRECTORY_SEPARATOR.$request->session()->get('albumDetails')['upload_ID']
                                       .DIRECTORY_SEPARATOR.'sample';
            $uploadPath = $assetPath;
            
            // We need an empty arry for us to put the files back into
            $results = array();

            foreach ($files as $file) 
            {    
                // store our uploaded file in our uploads folder
                $file->move($uploadPath, $file->getClientOriginalName());
                
                // set our results to have our asset path
                $name = $file->getClientOriginalName();
                $results[] = compact('name');
            }
            Session::push('albumDetails.sample_track_name', $results);
            // return our results in a files object
            return array(
                'files' => $results
            );
        }
        if($step == 4)
        {   
            // Grab our files input
            $files = Input::file('files');
                        
            // $assetPath = storage_path().'/uploads';
            $assetPath = storage_path().DIRECTORY_SEPARATOR.'uploads'
                                       .DIRECTORY_SEPARATOR.'books'
                                       .DIRECTORY_SEPARATOR.$data['user_ID']
                                       .DIRECTORY_SEPARATOR.$request->session()->get('albumDetails')['upload_ID']
                                       .DIRECTORY_SEPARATOR.'artwork';
            $uploadPath = $assetPath;
            
            // We need an empty arry for us to put the files back into
            $results = array();

            foreach ($files as $file) 
            {    
                // store our uploaded file in our uploads folder
                $file->move($uploadPath, $file->getClientOriginalName());
                
                // set our results to have our asset path
                $name = $file->getClientOriginalName();
                $results[] = compact('name');
            }
            Session::push('albumDetails.sample_track_name', $results);
            // return our results in a files object
            return array(
                'files' => $results
            );
        }

        if ($step == $this->lastStep) {

           \Session::flash('flash_message','Office successfully added.'); //<--FLASH MESSAGE

            return redirect()->action('uploadController@books_step_1');
        }

        return redirect()->action('uploadController@getBooksStep', ['step' => $step+1]);
    }
    public function getBooksDone(Request $request)
    {
        Mail::send('emails.upload_admin',(Array)Auth::user(), function($message)
        {
                        $message->from(\Config::get('const.NO_REPLY_EMAIL'), 'DIVVIY');
                        $message->to(\Config::get('const.ADMIN_EMAIL'))->subject('DIVVIY NEW UPLOAD');
        });

        Mail::send('emails.upload_user',(Array)Auth::user(), function($message)
        {
                        $message->from(\Config::get('const.NO_REPLY_EMAIL'), 'DIVVIY');
                        $message->to(Auth::user()->email_id)->subject('DIVVIY NEW UPLOAD');
        });
        \Session::flash('flash_message','Thank you! Your media has been sent for approval. We will review medias authenticity and publish live within 48 hours.'); //<--FLASH MESSAGE
        
        return Redirect::to('/upload/books/1');
    }

    public function musics()
    {
        $genre = categories_genre::where('option_ID','=',1)->lists('category_genre_name', 'category_genre_ID');

        $option_ID = 1;
        
        return view('uploads.musics',compact('genre','option_ID'));
    }
    
    public function musicsCreate()
    {
        dd('i am ahere');
        $rules = array(
                'artist_author_name'    => 'required',                        // just a normal required validation
                'category_genre_ID'     => 'required',     // required and must be unique in the ducks table
                'album_title_name'      => 'required',
                'upload_music'          => 'required',          // required and has to match the password field
                'upload_artwork'        => 'required',          // required and has to match the password field
                'upload_sample'         => 'required',          // required and has to match the password field
        );

         $validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) 
        {

            // get the error messages from the validator
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return Redirect::to('upload/musics')
                ->withErrors($validator);

        }
        
        $data = Input::all();

        $data['option_ID'] = 1;

        $data['uploaded_by'] = Auth::user()->user_ID;
        
        $insertedId = uploadMaster::create($data);                                
         
        $data['upload_ID'] = $insertedId->upload_ID;

        
        uploadAWSS3($data['upload_music'],'MUSIC/',$data['uploaded_by'],$data['upload_ID']);
        
        uploadAWSS3($data['upload_sample'],'MUSIC/',$data['uploaded_by'],$data['upload_ID'],'/SAMPLE/');

        uploadCover($data['upload_artwork'],'MUSIC/',$data['uploaded_by'],$data['upload_ID']);
        
        return Redirect::to('upload/musics');


    }
    public function postMusicsStep(Request $request, $step)
    {
        $data['user_ID'] =  Auth::user()->user_ID;

        switch ($step)
        {
            case 1:
                $rules = ['artist_author_name' => 'required',
                            'album_title_name' => 'required',
                            'album_price'      => 'required'];
                break;
            case 2:
                $rules = [];
                break;
            case 3:
                $rules = [];
                break;
            case 4:
                $rules = [];
                break;

            default:
                abort(400, "No rules for this step!");
        }

        $validate = $this->validate($request, $rules);

        if ($validate) 
        {
           $this->throwValidationException(
               $request, $validator
           );
        }

        /**
         * CREATE ALBUM DETAILS
         * @var [type]
         */
        if($step == 1)
        {
            $data['category_genre_ID'] = $request->input('category_genre_ID');

            $data['album_title_name'] = $request->input('album_title_name');

            $data['artist_author_name'] = $request->input('artist_author_name');

            $data['album_price'] = $request->input('album_price');

            $data['option_ID'] = 1;

            $data['is_active'] = 'N';
            
            $data['uploaded_by'] = Auth::user()->user_ID;

            $insertedId =  uploadMaster::create($data);

            $data['upload_ID'] = $insertedId->upload_ID;

            $albumDetails = ['album_title_name'  => $request->input('album_title_name'),
                             'option_ID'         => $data['option_ID'],
                             'category_genre_ID' => $request->input('category_genre_ID'),
                             'submitted_by'      => Auth::user()->user_ID,
                             'artist_author_name'=> $request->input('artist_author_name'),
                             'upload_ID'         => $data['upload_ID']];

            $request->session()->put('albumDetailsMusic', $albumDetails);

        }
        /**
         * UPLOAD MUSIC FILES
         * @var [type]
         */
        if($step == 2)
        {

            // Grab our files input
            $files = Input::file('files');
            
            // We will store our uploads in public/uploads/basic
            
            // $assetPath = storage_path().'/uploads';
            $assetPath = storage_path().DIRECTORY_SEPARATOR.'uploads'
                                       .DIRECTORY_SEPARATOR.'musics'
                                       .DIRECTORY_SEPARATOR.$data['user_ID']
                                       .DIRECTORY_SEPARATOR.$request->session()->get('albumDetailsMusic')['upload_ID']
                                       .DIRECTORY_SEPARATOR.'main';

            $uploadPath = $assetPath;
            
            // We need an empty arry for us to put the files back into
            $results = array();
            
            foreach ($files as $file) 
            {    
                // store our uploaded file in our uploads folder
                $file->move($uploadPath, $file->getClientOriginalName());
                
                // set our results to have our asset path
                $name = $file->getClientOriginalName();
                $results[] = compact('name');
            }
            
            // $request->session()->get('albumDetails')->update(Input::all());
            Session::push('albumDetailsMusic.track_name', $results);

            // return our results in a files object
            return array(
                'files' => $results
            );
            
        }

        
        /**
         * UPLOAD MUSIC SAMPLE
         * @var [type]
         */
        if($step == 3)
        {   
            // Grab our files input
            $files = Input::file('files');
            
            // We will store our uploads in public/uploads/basic
            
            // $assetPath = storage_path().'/uploads';
            $assetPath = storage_path().DIRECTORY_SEPARATOR.'uploads'
                                       .DIRECTORY_SEPARATOR.'musics'
                                       .DIRECTORY_SEPARATOR.$data['user_ID']
                                       .DIRECTORY_SEPARATOR.$request->session()->get('albumDetailsMusic')['upload_ID']
                                       .DIRECTORY_SEPARATOR.'sample';
            $uploadPath = $assetPath;
            
            // We need an empty arry for us to put the files back into
            $results = array();

            foreach ($files as $file) 
            {    
                // store our uploaded file in our uploads folder
                $file->move($uploadPath, $file->getClientOriginalName());
                
                // set our results to have our asset path
                $name = $file->getClientOriginalName();
                $results[] = compact('name');
            }
            Session::push('albumDetailsMusic.sample_track_name', $results);
            // return our results in a files object
            return array(
                'files' => $results
            );
        }
        if($step == 4)
        {   
            // Grab our files input
            $files = Input::file('files');
                        
            // $assetPath = storage_path().'/uploads';
            $assetPath = storage_path().DIRECTORY_SEPARATOR.'uploads'
                                       .DIRECTORY_SEPARATOR.'musics'
                                       .DIRECTORY_SEPARATOR.$data['user_ID']
                                       .DIRECTORY_SEPARATOR.$request->session()->get('albumDetailsMusic')['upload_ID']
                                       .DIRECTORY_SEPARATOR.'artwork';
            $uploadPath = $assetPath;
            
            // We need an empty arry for us to put the files back into
            $results = array();

            foreach ($files as $file) 
            {    
                // store our uploaded file in our uploads folder
                $file->move($uploadPath, $file->getClientOriginalName());
                
                // set our results to have our asset path
                $name = $file->getClientOriginalName();
                $results[] = compact('name');
            }
            Session::push('albumDetailsMusic.sample_track_name', $results);
            // return our results in a files object
            return array(
                'files' => $results
            );
        }

        if ($step == $this->lastStep) {

           \Session::flash('flash_message','Office successfully added.'); //<--FLASH MESSAGE

            return redirect()->action('uploadController@books_step_1');
        }

        return redirect()->action('uploadController@getMusicsStep', ['step' => $step+1]);
    }
    public function getMusicDone(Request $request)
    {
        Mail::send('emails.upload_admin',(Array)Auth::user(), function($message)
        {
                        $message->from(\Config::get('const.NO_REPLY_EMAIL'), 'DIVVIY');
                        $message->to(\Config::get('const.ADMIN_EMAIL'))->subject('Divviy new Upload Email');
        });

        Mail::send('emails.upload_user',(Array)Auth::user(), function($message)
        {
                        $message->from(\Config::get('const.NO_REPLY_EMAIL'), 'DIVVIY');
                        $message->to(Auth::user()->email_id)->subject('DIVVIY NEW UPLOAD');
        });
        \Session::flash('flash_message','Thank you! Your album has been sent for approval. We will review the album and contact you back soon  within 48 Hours.'); //<--FLASH MESSAGE
        
        return Redirect::to('/upload/musics/1');
    }
   
}
