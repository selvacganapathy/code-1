<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Input;
use App\user_master;
use App\user_password;
use App\user_address;
use App\user_cc;
use Hash;
use Redirect;
use Validator;
// use Request;
use App\Http\Requests;

class editprofileController extends Controller
{
    public function index($user_ID)
    {

    	 $userDetails = DB::table('user_master')
                    ->select('user_password.*', 'user_master.*','user_cc.*','user_address.*')
                    ->leftjoin('user_password', 'user_master.user_ID', '=', 'user_password.user_ID') 
                    ->leftjoin('user_cc', 'user_master.user_ID', '=', 'user_cc.user_ID')
                    ->leftjoin('user_address', 'user_master.user_ID', '=', 'user_address.user_ID')
                    ->where(function($query) use ($user_ID) {
                            $query->where('user_master.user_ID',$user_ID);
                    })
                    ->get();
        
        return view('editprofile',compact('userDetails'));
    }
    public function editprofile()
    {
    	user_address::where('user_ID',Input::get('user_ID'))
          				->update(['mobile_phone'=>Input::get('mobile_phone')]);

		user_master::where('user_ID',Input::get('user_ID'))
          				->update(['first_name'=>Input::get('first_name'),'last_name'=>Input::get('last_name')]);          				

   		\Session::flash('flash_message','Thank you! Your user profile has been updated.'); //<--FLASH MESSAGE

		return redirect()->action('editprofileController@index', [Input::get('user_ID')]);
    }
    public function editPassword()
    {
				$rules = array(
                'current_password'  => 'required',          // required and has to match the password field
                'new_password'    	=> 'required',          // required and has to match the password field
                'new_password1'     => 'required | same:new_password',          // required and has to match the password field
        	);
 				
 		$validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) 
        {

            // get the error messages from the validator
            $messages = $validator->messages();
            // redirect our user back to the form with the errors from the validator
            return Redirect::action('editprofileController@index', [Input::get('user_ID')])->withErrors($validator);

        }
    	
    	$currentPassword = user_password::where('user_ID',Input::get('user_ID'))->get();

    	if (Hash::check(Input::get('current_password'), $currentPassword[0]->password)) {
    			
    			user_password::where('user_ID',Input::get('user_ID'))
          				->update(['password'=>Hash::make(Input::get('new_password'))]); 

        \Session::flash('flash_message','Thank you! Your password has been updated, Please login to continue'); //<--FLASH MESSAGE

			Auth::logout();
    	return Redirect::to('/');
		}
		else
		{
			return Redirect::action('editprofileController@index', [Input::get('user_ID')])
                       ->withErrors([
                    'username' => 'Your password does not matched ',
                ]);
		}
    }
    public function editPayment()
    {
        $rules = array(
                'email'     => 'required | email',          // required and has to match the password field
            );
                
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) 
        {

            // get the error messages from the validator
            $messages = $validator->messages();
            // redirect our user back to the form with the errors from the validator
            return Redirect::action('editprofileController@index', [Input::get('user_ID')])->withErrors($validator);

        }
    	$payment_details = user_cc::where('user_ID',Input::get('user_ID'))->get();

    	if(count($payment_details) == 0)
    	{
    		user_cc::create(['email'=>Input::get('email'),
          					  'payment_options_id'=>Input::get('payment_options_id')	,
          					  'user_ID'=>Input::get('user_ID')]); 
    	}
    	else
    	{
    		user_cc::where('user_ID',Input::get('user_ID'))
          			->update(['email'=>Input::get('email'),'payment_options_id'=>Input::get('payment_options_id')]); 

    	}

        \Session::flash('flash_message','Thank you! Your paypal has been updated'); //<--FLASH MESSAGE

		return redirect()->action('editprofileController@index', [Input::get('user_ID')]);

    }

}
