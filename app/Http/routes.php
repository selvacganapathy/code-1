<?php
/**
 * ADMIN CONTROLLER - AUTHORISED - IMPLEMENT PAGINATION 
 * ADMIN CONTROLLER - APPROVED - IMPLEMENT PAGINATION 
 * ADMIN CONTROLLER - USER MEDIA - IMPLEMENT PAGINATION 
 * aws structure - /option/userid/uploadID/(main|sample|artwork)/filename
 */
// LOGIN AND REGISTRATION
Route::get('/', function () {

    if(Auth::check()) {
        return redirect('/home');
    } else {
        return view('auth.login');
    }
    return view('auth.login');
});

Route::get('/login', function () {
    return view('auth.login');
});

//HOME CONTROLLER
Route::get('home','homeController@index');

//UPLOAD CONTROLLER
Route::get('upload','uploadController@index');
Route::get('upload/books/{step}', 'uploadController@getBooksStep')->where(['step' => '[1-4]']);
Route::post('upload/books/{step}', 'uploadController@postBooksStep')->where(['step' => '[1-4]']);
Route::get('upload/booksdone', 'uploadController@getBooksDone');

Route::get('upload/musics/{step}', 'uploadController@getMusicsStep')->where(['step' => '[1-4]']);
Route::post('upload/musics/{step}', 'uploadController@postMusicsStep')->where(['step' => '[1-4]']);
Route::get('upload/musicsdone', 'uploadController@getBooksDone');

//FEED CONTROLLER
Route::get('feed','feedController@index');
// Route::get('feed/{user_ID}','feedController@edit');
Route::get('feed/{upload_ID}','feedController@getAlbumList');
Route::post('feed','feedController@changeProfilePic');

//EDIT PROFILE CONTROLLER
Route::get('edit/{user_ID}','editprofileController@index');
Route::post('editprofile','editprofileController@editprofile');
Route::post('editPassword','editprofileController@editPassword');
Route::post('editPayment','editprofileController@editPayment');


//ADMINCONTROLLER
Route::get('admin','adminController@index');
Route::post('admin/{id}',['uses' => 'adminController@authorise']);
Route::get('admin/approved',['uses' => 'adminController@approved']);
Route::get('admin/approved/{user_ID}',['uses' => 'adminController@userMedia']);
Route::get('admin/approved/{option}/{user_ID}/{upload_ID}',['uses' => 'adminController@viewAlbum']);
Route::get('downloadMedia/{options}/{user_ID}/{upload_ID}/{filename}/{option?}',['uses' => 'adminController@downloadAlbum']);
Route::post('admin/approved/{user_ID}',['uses' => 'adminController@userMediaApprove']);

Route::get('download/{userid}/{option}/{filename}', 'adminController@download');
Route::get('downloads/{user_ID}/{filename}', function($id,$filename)
{
	/**
	 * download file from s3 - incomplete
	 */
	$disk = Storage::disk('s3');
   
	$exists = Storage::disk('s3')->has('/REGISTER/'.$id.'/'.$filename);

    if($exists)
    {
    	//$url = Storage::get('/REGISTER/'.$id.'/'.$filename);
    	 $headers = [
        'Content-Disposition' => 'attachment',
        'filename'=>$filename
    	];
    	return response()->download('/REGISTER/'.$id.'/'.$filename, $filename, $headers);
    }
});
Route::get('terms/{filename}', function($filename)
{
    $file = storage_path('docs') . '/' . $filename;
    
    if (File::isFile($file))
        {
            $file = File::get($file);
            $response = Response::make($file, 200);
            $content_types = [
                'application/octet-stream', // txt etc
                'application/msword', // doc
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document', //docx
                'application/vnd.ms-excel', // xls
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // xlsx
                'application/pdf', // pdf
            ];
            // using this will allow you to do some checks on it (if pdf/docx/doc/xls/xlsx)
            $response->header('Content-Type', $content_types);

            return $response;
    }
});

Route::get('contact/email','contactController@index');

Route::post('contact/email','contactController@index');

Route::post('postcode/{postcode}',function($postcode){
 if(Request::ajax()) 
    {
        $search = Postcode::lookup($postcode);
         return $search;
    }
});

//LOGOUT CONTROLLER
Route::get('/logout',function(){

    Auth::logout();
    return Redirect::to('/');
});