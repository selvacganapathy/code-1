<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categories_genre extends Model
{
    protected $primaryKey = 'category_genre_ID';
    
    protected $table = 'categories_genre';   

    protected $fillable = [
    	'category_genre_name',
    	'option_ID',
    	'is_active',
    ];
}
