# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.33.10 (MySQL 5.1.71)
# Database: divvyaws
# Generation Time: 2016-04-15 16:55:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table categories_genre
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories_genre`;

CREATE TABLE `categories_genre` (
  `category_genre_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_genre_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option_ID` int(10) unsigned NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_genre_ID`),
  KEY `categories_genre_option_id_foreign` (`option_ID`),
  CONSTRAINT `categories_genre_option_id_foreign` FOREIGN KEY (`option_ID`) REFERENCES `options_masters` (`option_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `categories_genre` WRITE;
/*!40000 ALTER TABLE `categories_genre` DISABLE KEYS */;

INSERT INTO `categories_genre` (`category_genre_ID`, `category_genre_name`, `option_ID`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'Alternative Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(2,'Blues',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(3,'Classical Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(4,'Country Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(5,'Dance Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(6,'Easy Listening',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(7,'Electronic Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(8,'European Music (Folk / Pop)',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(9,'Hip Hop / Rap',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(10,'Indie Pop',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(11,'Grime',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(12,'Inspirational (incl. Gospel)',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(13,'Asian Pop (J-Pop, K-pop)',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(14,'Jazz',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(15,'Latin Music',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(16,'New Age',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(17,'Opera',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(18,'Pop (Popular music)',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(19,'R&B / Soul',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(20,'Reggae',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(21,'Rock',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(22,'Singer / Songwriter (inc. Folk)',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(23,'World Music / Beats',1,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(24,'Mystery',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(25,'Horror',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(26,'Health',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(27,'Children\'s',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(28,'History',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(29,'Poetry',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(30,'Art',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00'),
	(31,'Action',2,'Y','2016-03-26 11:36:00','2016-03-26 11:36:00');

/*!40000 ALTER TABLE `categories_genre` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cloud_urls
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cloud_urls`;

CREATE TABLE `cloud_urls` (
  `cloud_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `base_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cloud_type` enum('DEVELOPMENT','LIVE') COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cloud_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `cloud_urls` WRITE;
/*!40000 ALTER TABLE `cloud_urls` DISABLE KEYS */;

INSERT INTO `cloud_urls` (`cloud_ID`, `base_url`, `cloud_type`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'https://s3-eu-west-1.amazonaws.com/divviyaws','DEVELOPMENT','Y',NULL,NULL);

/*!40000 ALTER TABLE `cloud_urls` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`migration`, `batch`)
VALUES
	('2016_03_20_120214_create_user_master_table',1),
	('2016_03_20_120313_create_user_address_table',1),
	('2016_03_20_120323_create_user_cc_table',1),
	('2016_03_20_120334_create_user_password_table',1),
	('2016_03_20_120426_create_user_photo_bill_table',1),
	('2016_03_26_093647_create_options_masters_table',1),
	('2016_03_26_093726_create_categories_table',1),
	('2016_03_26_094740_create_upload_masters_table',1),
	('2016_03_26_094818_create_url_masters_table',1),
	('2016_03_26_104222_create_cloud_urls_table',1),
	('2016_03_20_120214_create_user_master_table',1),
	('2016_03_20_120313_create_user_address_table',1),
	('2016_03_20_120323_create_user_cc_table',1),
	('2016_03_20_120334_create_user_password_table',1),
	('2016_03_20_120426_create_user_photo_bill_table',1),
	('2016_03_26_093647_create_options_masters_table',1),
	('2016_03_26_093726_create_categories_table',1),
	('2016_03_26_094740_create_upload_masters_table',1),
	('2016_03_26_094818_create_url_masters_table',1),
	('2016_03_26_104222_create_cloud_urls_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table options_masters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `options_masters`;

CREATE TABLE `options_masters` (
  `option_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`option_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `options_masters` WRITE;
/*!40000 ALTER TABLE `options_masters` DISABLE KEYS */;

INSERT INTO `options_masters` (`option_ID`, `option_name`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'MUSICS','Y',NULL,NULL),
	(2,'BOOKS','Y',NULL,NULL);

/*!40000 ALTER TABLE `options_masters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `userID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projectRef` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `projectNE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `projectNO` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `projectShow` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `systemID` int(11) NOT NULL,
  `projectName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prioritiesID` int(11) NOT NULL,
  `projectStatusID` int(11) NOT NULL,
  `suppliersID` int(11) NOT NULL,
  `departmentsID` int(11) NOT NULL,
  `usersID` int(11) NOT NULL,
  `projectManagerID` int(11) NOT NULL,
  `readerID` int(11) NOT NULL,
  `projectStage` enum('DISCOVER','PLAN','DO','IMPLEMENT','CLOSE') COLLATE utf8_unicode_ci NOT NULL,
  `projectMood` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `planDISCOVERStartDate` datetime DEFAULT NULL,
  `actualDISCOVERStartDate` datetime DEFAULT NULL,
  `planStartDate` datetime DEFAULT NULL,
  `actualStartDate` datetime DEFAULT NULL,
  `planDOStartDate` datetime DEFAULT NULL,
  `actualDOStartDate` datetime DEFAULT NULL,
  `planIMPLEMENTStartDate` datetime DEFAULT NULL,
  `actualIMPLEMENTStartDate` datetime DEFAULT NULL,
  `planCOMPLETEStartDate` datetime DEFAULT NULL,
  `actualCOMPLETEStartDate` datetime DEFAULT NULL,
  `planDISCOVERDays` int(11) NOT NULL,
  `actualDISCOVERDays` int(11) NOT NULL,
  `planDays` int(11) NOT NULL,
  `actualDays` int(11) NOT NULL,
  `planDODays` int(11) NOT NULL,
  `actualDODays` int(11) NOT NULL,
  `planIMPLEMENTDays` int(11) NOT NULL,
  `actualIMPLEMENTDays` int(11) NOT NULL,
  `planCOMPLETEDays` int(11) NOT NULL,
  `actualCOMPLETEDays` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table upload_masters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `upload_masters`;

CREATE TABLE `upload_masters` (
  `upload_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_ID` int(10) unsigned NOT NULL,
  `category_genre_ID` int(10) unsigned NOT NULL,
  `uploaded_by` int(11) NOT NULL,
  `album_title_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `artist_author_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `track_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`upload_ID`),
  KEY `upload_masters_option_id_foreign` (`option_ID`),
  KEY `upload_masters_category_genre_id_foreign` (`category_genre_ID`),
  CONSTRAINT `upload_masters_category_genre_id_foreign` FOREIGN KEY (`category_genre_ID`) REFERENCES `categories_genre` (`category_genre_ID`),
  CONSTRAINT `upload_masters_option_id_foreign` FOREIGN KEY (`option_ID`) REFERENCES `options_masters` (`option_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `upload_masters` WRITE;
/*!40000 ALTER TABLE `upload_masters` DISABLE KEYS */;

INSERT INTO `upload_masters` (`upload_ID`, `option_ID`, `category_genre_ID`, `uploaded_by`, `album_title_name`, `artist_author_name`, `track_name`, `is_active`, `created_at`, `updated_at`)
VALUES
	(4,1,1,1,'sky','beyonce','test','Y','2016-03-26 18:55:42','2016-03-26 18:55:42'),
	(5,1,13,1,'test','jls','test','Y','2016-03-27 10:04:44','2016-03-27 10:04:44'),
	(6,1,8,1,'american','beyonce','test','Y','2016-03-27 10:18:58','2016-03-27 10:18:58'),
	(7,1,8,1,'american','beyonce','test','Y','2016-03-27 10:19:25','2016-03-27 10:19:25'),
	(8,1,8,1,'american','beyonce','test','Y','2016-03-27 10:20:14','2016-03-27 10:20:14'),
	(9,1,3,1,'selvaAlbum','selvaArtist','test','Y','2016-03-27 10:21:59','2016-03-27 10:21:59'),
	(10,1,3,1,'selvaAlbum','selvaArtist','test','Y','2016-03-27 10:25:00','2016-03-27 10:25:00'),
	(11,1,3,1,'selvaAlbum','selvaArtist','test','Y','2016-03-27 10:31:00','2016-03-27 10:31:00'),
	(12,1,1,1,'test','beyonce','','Y','2016-03-27 11:21:29','2016-03-27 11:21:29'),
	(13,1,5,1,'american','beyonce','','Y','2016-03-27 11:30:04','2016-03-27 11:30:04'),
	(14,1,14,1,'american','pitbull','','Y','2016-03-27 11:32:18','2016-03-27 11:32:18'),
	(15,1,9,1,'american','beyonce','','Y','2016-03-27 11:36:14','2016-03-27 11:36:14'),
	(16,1,1,1,'test','beyonce','','Y','2016-03-27 11:37:39','2016-03-27 11:37:39'),
	(17,1,1,1,'american','beyonce','','Y','2016-03-27 16:03:49','2016-03-27 16:03:49'),
	(18,1,1,1,'american','beyonce','','Y','2016-03-27 16:04:39','2016-03-27 16:04:39'),
	(22,2,24,0,'','','','Y','2016-04-14 15:39:36','2016-04-14 18:38:29'),
	(23,2,24,0,'sdfdsf','sdfsdf','','Y','2016-04-15 09:57:11','2016-04-15 09:57:11');

/*!40000 ALTER TABLE `upload_masters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table url_masters
# ------------------------------------------------------------

DROP TABLE IF EXISTS `url_masters`;

CREATE TABLE `url_masters` (
  `url_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url_params` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `upload_ID` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`url_ID`),
  KEY `url_masters_upload_id_foreign` (`upload_ID`),
  CONSTRAINT `url_masters_upload_id_foreign` FOREIGN KEY (`upload_ID`) REFERENCES `upload_masters` (`upload_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `url_masters` WRITE;
/*!40000 ALTER TABLE `url_masters` DISABLE KEYS */;

INSERT INTO `url_masters` (`url_ID`, `url_params`, `full_url`, `upload_ID`, `created_at`, `updated_at`)
VALUES
	(1,'/MUSICFILE/','1459018542_fGbAIZ1hactnIHXiGPF0y9IxvdFR2Pmh.pdf',4,'2016-03-26 18:55:48','2016-03-26 18:55:48'),
	(2,'/MUSICFILE/','1459018548_toOneE0Q2SQmhCQY8WTdVC0aWmxKoXoD.pdf',4,'2016-03-26 18:55:52','2016-03-26 18:55:52'),
	(3,'/SAMPLE/','1459018552_Qmz8x1ZhKCSF5V2FcHu34WLSmxj9ITPu.pdf',4,'2016-03-26 18:55:53','2016-03-26 18:55:53'),
	(4,'/SAMPLE/','1459018553_ysqvl0WgyIHNRKx5hOYA94tHcLZQNTMg.pdf',4,'2016-03-26 18:55:54','2016-03-26 18:55:54');

/*!40000 ALTER TABLE `url_masters` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_address
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_address`;

CREATE TABLE `user_address` (
  `address_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned NOT NULL,
  `line1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `town_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `home_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`address_ID`),
  KEY `user_address_user_id_foreign` (`user_ID`),
  CONSTRAINT `user_address_user_id_foreign` FOREIGN KEY (`user_ID`) REFERENCES `user_master` (`user_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_address` WRITE;
/*!40000 ALTER TABLE `user_address` DISABLE KEYS */;

INSERT INTO `user_address` (`address_ID`, `user_ID`, `line1`, `line2`, `town_city`, `country`, `zipcode`, `home_phone`, `mobile_phone`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,1,'58 ','terry road','coventry','UK','cv12ba','07584165042','07584165042','Y','2016-03-26 11:24:10','2016-03-26 11:24:10'),
	(19,12220,'58','test address 2','coventry','UK','cv12ba','07584165042','07584165042','Y','2016-04-10 21:31:57','2016-04-10 21:31:57');

/*!40000 ALTER TABLE `user_address` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_cc
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_cc`;

CREATE TABLE `user_cc` (
  `user_cc_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned NOT NULL,
  `card_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `security_code` int(11) NOT NULL,
  `sort_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_cc_ID`),
  KEY `user_cc_user_id_foreign` (`user_ID`),
  CONSTRAINT `user_cc_user_id_foreign` FOREIGN KEY (`user_ID`) REFERENCES `user_master` (`user_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_cc` WRITE;
/*!40000 ALTER TABLE `user_cc` DISABLE KEYS */;

INSERT INTO `user_cc` (`user_cc_ID`, `user_ID`, `card_name`, `card_number`, `security_code`, `sort_code`, `account_number`, `expiry_date`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,1,'s chinnakalai','8786567761235467',212,'564477','12121212','08/16','Y','2016-03-26 11:24:10','2016-03-26 11:24:10'),
	(19,12220,'s chinnakalai','8786567761235467',212,'564477','12121212','08/16','Y','2016-04-10 21:31:57','2016-04-10 21:31:57');

/*!40000 ALTER TABLE `user_cc` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_master
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_master`;

CREATE TABLE `user_master` (
  `user_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` enum('MALE','FEMALE') COLLATE utf8_unicode_ci NOT NULL,
  `origanisation` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_ID`),
  UNIQUE KEY `user_master_email_id_unique` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_master` WRITE;
/*!40000 ALTER TABLE `user_master` DISABLE KEYS */;

INSERT INTO `user_master` (`user_ID`, `first_name`, `last_name`, `email_id`, `date_of_birth`, `gender`, `origanisation`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'selvaganapathy','chinnakalai','selvacganapathy@gmail.com','0000-00-00','MALE','scube software solutions','Y','2016-03-26 11:24:10','2016-03-26 11:24:10'),
	(12220,'selvaganapathy','chinnakalai','selva@scubess.com','0000-00-00','MALE','scube software solutions','Y','2016-04-10 21:31:57','2016-04-10 21:53:15');

/*!40000 ALTER TABLE `user_master` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_password
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_password`;

CREATE TABLE `user_password` (
  `pwd_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_ID` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` enum('ADMIN','GENERAL','CLIENT') COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pwd_ID`),
  UNIQUE KEY `user_password_email_id_unique` (`email_id`),
  CONSTRAINT `user_password_email_id_foreign` FOREIGN KEY (`email_id`) REFERENCES `user_master` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_password` WRITE;
/*!40000 ALTER TABLE `user_password` DISABLE KEYS */;

INSERT INTO `user_password` (`pwd_ID`, `email_id`, `remember_token`, `user_ID`, `password`, `user_type`, `is_active`, `created_at`, `updated_at`)
VALUES
	(1,'selvacganapathy@gmail.com','FmujBe5TOKV1pJLn357vTaLWAY1VDSfeom9Vap9oGcYSJ7cjeurrz0vAmlDL',1,'$2y$10$TOdO1Qfznk6gIbvHVhNpEemelgwBYQWPD83i6gCovl0uLUqj5tZ1u','ADMIN','Y','2016-03-26 11:24:10','2016-04-15 16:29:27'),
	(21,'selva@scubess.com','',12220,'$2y$10$iuZNIB4X7dxGeDfxKE2XHOjq8DggqQ625AHbLnyY4ZOIUdEwTD4cq','CLIENT','Y','2016-04-10 21:31:57','2016-04-10 21:31:57');

/*!40000 ALTER TABLE `user_password` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_photo_bill
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_photo_bill`;

CREATE TABLE `user_photo_bill` (
  `upload_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_ID` int(10) unsigned NOT NULL,
  `utility_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`upload_ID`),
  KEY `user_photo_bill_user_id_foreign` (`user_ID`),
  CONSTRAINT `user_photo_bill_user_id_foreign` FOREIGN KEY (`user_ID`) REFERENCES `user_master` (`user_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_photo_bill` WRITE;
/*!40000 ALTER TABLE `user_photo_bill` DISABLE KEYS */;

INSERT INTO `user_photo_bill` (`upload_ID`, `user_ID`, `utility_url`, `photo_url`, `is_active`, `created_at`, `updated_at`)
VALUES
	(19,12220,'UTILITYBILL_12220.pdf','IMAGE_12220.jpeg','Y','2016-04-10 21:31:57','2016-04-10 21:31:57'),
	(20,12220,'','REGISTER/12220/IMAGE_12220.jpeg','Y','2016-04-10 21:38:49','2016-04-10 21:38:49'),
	(21,12220,'','REGISTER/12220/UTILITYBILL_12220.pdf','Y','2016-04-10 21:38:51','2016-04-10 21:38:51');

/*!40000 ALTER TABLE `user_photo_bill` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `usersID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photourl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `utilitybillurl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isDeleted` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `deletedDate` datetime DEFAULT NULL,
  `validated` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `validatedDate` datetime DEFAULT NULL,
  `usertype` enum('admin','superadmin','client','general') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`usersID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
