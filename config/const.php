<?php

return [
        /**
        *   UPLOAD LIMITS 
        */
        'PROFILE_IMAGE_SIZE'      		=>      '5000000',       // 5 MB
        'UTILITY_FILE_SIZE'      		=>      '50000000',      // 10 MB
        'SAMPLE_BOOK_SIZE'      		=>      '50000000',      // 50 MB
        'SAMPLE_AUDIO_SIZE'      		=>      '10000000',      // 10 MB
        'ORIGINAL_BOOK_SIZE'      		=>      '350000000',     // 350 MB
        'ORIGINAL_AUDIO_SIZE'           =>      '12000000',      // 12 MB
        'ARTWORK_IMAGE_SIZE'      		=>      '5000000',       // 5 MB

        /**
         * S3 BUCKET 
         */
        'AWS_STORAGE_URL'				=>		'https://s3-eu-west-1.amazonaws.com/divviyaws',
        'AWS_STORAGE_URL_REGISTER'      =>      '/register/',
        'AWS_STORAGE_URL_BOOKS'         =>      '/books/',
        'AWS_STORAGE_URL_MUSIC'         =>      '/musics/',

        /**
         * LOCAL
         */
        'LOCAL_STORAGE_URL'             =>      '',
        'LOCAL_STORAGE_URL_REGISTER'    =>      '/uploads/register/',
        'LOCAL_STORAGE_URL_BOOKS'       =>      '/uploads/books/',
        'LOCAL_STORAGE_URL_MUSICS'       =>      '/uploads/musics/',

        /**
         * CONFIGURE EMAIL
         */
        'CONTACT_FORM_EMAIL'            =>      'admin@divviy.com',
        'ADMIN_EMAIL'			        =>		'admin@divviy.com',
        'CONTACT_SUPPORT_EMAIL'         =>      'support@divviy.com',
        'NEW_REGISTRATION_EMAIL'	    =>		'admin@divviy.com',
        'NO_REPLY_EMAIL'                =>      'no-reply@divviy.com',
        'HELPDESK_EMAIL'                =>      'helpdesk@divviy.com',
        /**
         * PAYMENT OPTION
         */
        /**
         * CONFIGURE LOCALS 
         */
        'REGISTER_MAX_ATTACHMENT'       =>      2,
        'MAX_DISPLAY_FEED_MUSIC'        =>      100,
        'MAX_DISPLAY_FEED_BOOKS'        =>      100,

    ];  

?>